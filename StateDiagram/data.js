let Data = {
  "states": [
    {
      "id": "*",
      "state": []
    },
    {
      "id": "4",
      "state": [
        {
          "instance": "driver",
          "state": [
            {
              "name": "state",
              "value": "State:Acquire"
            }
          ]
        },
        {
          "instance": "optics.optics",
          "state": [
            {
              "name": "state",
              "value": "State:Idle"
            },
            {
              "name": "ready",
              "value": "false"
            }
          ]
        },
        {
          "instance": "optics.focus.focus",
          "state": [
            {
              "name": "idle",
              "value": "true"
            },
            {
              "name": "contrast_was",
              "value": "EContrast:Blurrier"
            }
          ]
        },
        {
          "instance": "optics.focus.lens.port",
          "state": [
            {
              "name": "moving",
              "value": "false"
            }
          ]
        },
        {
          "instance": "acquisition.acquire",
          "state": [
            {
              "name": "state",
              "value": "State:Acquiring"
            }
          ]
        },
        {
          "instance": "acquisition.sensor.port",
          "state": [
            {
              "name": "state",
              "value": "State:Acquire"
            }
          ]
        }
      ]
    },
    {
      "id": "6",
      "state": [
        {
          "instance": "driver",
          "state": [
            {
              "name": "state",
              "value": "State:Setup"
            }
          ]
        },
        {
          "instance": "optics.optics",
          "state": [
            {
              "name": "state",
              "value": "State:Prepare"
            },
            {
              "name": "ready",
              "value": "false"
            }
          ]
        },
        {
          "instance": "optics.focus.focus",
          "state": [
            {
              "name": "idle",
              "value": "false"
            },
            {
              "name": "contrast_was",
              "value": "EContrast:Blurrier"
            }
          ]
        },
        {
          "instance": "optics.focus.lens.port",
          "state": [
            {
              "name": "moving",
              "value": "true"
            }
          ]
        },
        {
          "instance": "acquisition.acquire",
          "state": [
            {
              "name": "state",
              "value": "State:Ready"
            }
          ]
        },
        {
          "instance": "acquisition.sensor.port",
          "state": [
            {
              "name": "state",
              "value": "State:Ready"
            }
          ]
        }
      ]
    },
    {
      "id": "1",
      "state": [
        {
          "instance": "driver",
          "state": [
            {
              "name": "state",
              "value": "State:Idle"
            }
          ]
        },
        {
          "instance": "optics.optics",
          "state": [
            {
              "name": "state",
              "value": "State:Idle"
            },
            {
              "name": "ready",
              "value": "false"
            }
          ]
        },
        {
          "instance": "optics.focus.focus",
          "state": [
            {
              "name": "idle",
              "value": "true"
            },
            {
              "name": "contrast_was",
              "value": "EContrast:Blurrier"
            }
          ]
        },
        {
          "instance": "optics.focus.lens.port",
          "state": [
            {
              "name": "moving",
              "value": "false"
            }
          ]
        },
        {
          "instance": "acquisition.acquire",
          "state": [
            {
              "name": "state",
              "value": "State:Idle"
            }
          ]
        },
        {
          "instance": "acquisition.sensor.port",
          "state": [
            {
              "name": "state",
              "value": "State:Idle"
            }
          ]
        }
      ]
    },
    {
      "id": "7",
      "state": [
        {
          "instance": "driver",
          "state": [
            {
              "name": "state",
              "value": "State:Acquire"
            }
          ]
        },
        {
          "instance": "optics.optics",
          "state": [
            {
              "name": "state",
              "value": "State:Idle"
            },
            {
              "name": "ready",
              "value": "false"
            }
          ]
        },
        {
          "instance": "optics.focus.focus",
          "state": [
            {
              "name": "idle",
              "value": "true"
            },
            {
              "name": "contrast_was",
              "value": "EContrast:Sharper"
            }
          ]
        },
        {
          "instance": "optics.focus.lens.port",
          "state": [
            {
              "name": "moving",
              "value": "false"
            }
          ]
        },
        {
          "instance": "acquisition.acquire",
          "state": [
            {
              "name": "state",
              "value": "State:Acquiring"
            }
          ]
        },
        {
          "instance": "acquisition.sensor.port",
          "state": [
            {
              "name": "state",
              "value": "State:Acquire"
            }
          ]
        }
      ]
    },
    {
      "id": "5",
      "state": [
        {
          "instance": "driver",
          "state": [
            {
              "name": "state",
              "value": "State:Idle"
            }
          ]
        },
        {
          "instance": "optics.optics",
          "state": [
            {
              "name": "state",
              "value": "State:Idle"
            },
            {
              "name": "ready",
              "value": "false"
            }
          ]
        },
        {
          "instance": "optics.focus.focus",
          "state": [
            {
              "name": "idle",
              "value": "true"
            },
            {
              "name": "contrast_was",
              "value": "EContrast:Sharper"
            }
          ]
        },
        {
          "instance": "optics.focus.lens.port",
          "state": [
            {
              "name": "moving",
              "value": "false"
            }
          ]
        },
        {
          "instance": "acquisition.acquire",
          "state": [
            {
              "name": "state",
              "value": "State:Idle"
            }
          ]
        },
        {
          "instance": "acquisition.sensor.port",
          "state": [
            {
              "name": "state",
              "value": "State:Idle"
            }
          ]
        }
      ]
    },
    {
      "id": "2",
      "state": [
        {
          "instance": "driver",
          "state": [
            {
              "name": "state",
              "value": "State:Setup"
            }
          ]
        },
        {
          "instance": "optics.optics",
          "state": [
            {
              "name": "state",
              "value": "State:Prepare"
            },
            {
              "name": "ready",
              "value": "false"
            }
          ]
        },
        {
          "instance": "optics.focus.focus",
          "state": [
            {
              "name": "idle",
              "value": "false"
            },
            {
              "name": "contrast_was",
              "value": "EContrast:Sharper"
            }
          ]
        },
        {
          "instance": "optics.focus.lens.port",
          "state": [
            {
              "name": "moving",
              "value": "true"
            }
          ]
        },
        {
          "instance": "acquisition.acquire",
          "state": [
            {
              "name": "state",
              "value": "State:Ready"
            }
          ]
        },
        {
          "instance": "acquisition.sensor.port",
          "state": [
            {
              "name": "state",
              "value": "State:Ready"
            }
          ]
        }
      ]
    },
    {
      "id": "3",
      "state": [
        {
          "instance": "driver",
          "state": [
            {
              "name": "state",
              "value": "State:Ready"
            }
          ]
        },
        {
          "instance": "optics.optics",
          "state": [
            {
              "name": "state",
              "value": "State:Ready"
            },
            {
              "name": "ready",
              "value": "false"
            }
          ]
        },
        {
          "instance": "optics.focus.focus",
          "state": [
            {
              "name": "idle",
              "value": "true"
            },
            {
              "name": "contrast_was",
              "value": "EContrast:Blurrier"
            }
          ]
        },
        {
          "instance": "optics.focus.lens.port",
          "state": [
            {
              "name": "moving",
              "value": "false"
            }
          ]
        },
        {
          "instance": "acquisition.acquire",
          "state": [
            {
              "name": "state",
              "value": "State:Ready"
            }
          ]
        },
        {
          "instance": "acquisition.sensor.port",
          "state": [
            {
              "name": "state",
              "value": "State:Ready"
            }
          ]
        }
      ]
    }
  ],
  "transitions": [
    {
      "from": "*", "to": "1",
      "trigger": "",
      "action": [""]
    },

    
    {
      "from": "1", "to": "2",
      "trigger": "control.setup",
      "action": ["acquisition.sensor.port.prepare", "acquisition.sensor.port.return", "optics.focus.sensor.port.measure", "optics.focus.sensor.port.EContrast:Sharper", "optics.focus.lens.port.forward", "optics.focus.lens.port.return", "control.return"]
    },
    {
      "from": "1", "to": "6",
      "trigger": "control.setup",
      "action": ["acquisition.sensor.port.prepare", "acquisition.sensor.port.return", "optics.focus.sensor.port.measure", "optics.focus.sensor.port.EContrast:Blurrier", "optics.focus.lens.port.forward", "optics.focus.lens.port.return", "control.return"]
    },

    
    {
      "from": "2", "to": "2",
      "trigger": "optics.focus.lens.port.stopped",
      "action": ["optics.focus.sensor.port.measure", "optics.focus.sensor.port.EContrast:Sharper", "optics.focus.lens.port.forward", "optics.focus.lens.port.return"]
    },
    {
      "from": "2", "to": "3",
      "trigger": "optics.focus.lens.port.stopped",
      "action": ["optics.focus.sensor.port.measure", "optics.focus.sensor.port.EContrast:Blurrier", "control.focus_lock"]
    },


    {
      "from": "2", "to": "5",
      "trigger": "control.release",
      "action": ["acquisition.sensor.port.cancel", "acquisition.sensor.port.return", "optics.focus.lens.port.stop", "optics.focus.lens.port.return", "control.return"]
    },


    {
      "from": "2", "to": "7",
      "trigger": "control.shoot",
      "action": ["acquisition.sensor.port.acquire", "acquisition.sensor.port.return", "optics.focus.lens.port.stop", "optics.focus.lens.port.return", "optics.shutter.port.expose", "optics.shutter.port.return", "control.return"]
    },


    {
      "from": "3", "to": "1",
      "trigger": "control.release",
      "action": ["acquisition.sensor.port.cancel", "acquisition.sensor.port.return", "optics.focus.lens.port.stop", "optics.focus.lens.port.return", "control.return"]
    },


    {
      "from": "3", "to": "4",
      "trigger": "control.shoot",
      "action": ["acquisition.sensor.port.acquire", "acquisition.sensor.port.return", "optics.shutter.port.expose", "optics.shutter.port.return", "control.return"]
    },


    {
      "from": "4", "to": "1",
      "trigger": "acquisition.sensor.port.image",
      "action": ["acquisition.memory.port.store", "acquisition.memory.port.return", "control.image"]
    },


    {
      "from": "5", "to": "2",
      "trigger": "control.setup",
      "action": ["acquisition.sensor.port.prepare", "acquisition.sensor.port.return", "optics.focus.sensor.port.measure", "optics.focus.sensor.port.EContrast:Sharper", "optics.focus.lens.port.forward", "optics.focus.lens.port.return", "control.return"]
    },
    {
      "from": "5", "to": "6",
      "trigger": "control.setup",
      "action": ["acquisition.sensor.port.prepare", "acquisition.sensor.port.return", "optics.focus.sensor.port.measure", "optics.focus.sensor.port.EContrast:Blurrier", "optics.focus.lens.port.forward", "optics.focus.lens.port.return", "control.return"]
    },


    {
      "from": "6", "to": "1",
      "trigger": "control.release",
      "action": ["acquisition.sensor.port.cancel", "acquisition.sensor.port.return", "optics.focus.lens.port.stop", "optics.focus.lens.port.return", "control.return"]
    },


    {
      "from": "6", "to": "2",
      "trigger": "optics.focus.lens.port.stopped",
      "action": ["optics.focus.sensor.port.measure", "optics.focus.sensor.port.EContrast:Sharper", "optics.focus.lens.port.backward", "optics.focus.lens.port.return"]
    },
    {
      "from": "6", "to": "6",
      "trigger": "optics.focus.lens.port.stopped",
      "action": ["optics.focus.sensor.port.measure", "optics.focus.sensor.port.EContrast:Blurrier", "optics.focus.lens.port.backward", "optics.focus.lens.port.return"]
    },


    {
      "from": "6", "to": "4",
      "trigger": "control.shoot",
      "action": ["acquisition.sensor.port.acquire", "acquisition.sensor.port.return", "optics.focus.lens.port.stop", "optics.focus.lens.port.return", "optics.shutter.port.expose", "optics.shutter.port.return", "control.return"]
    },


    {
      "from": "7", "to": "5",
      "trigger": "acquisition.sensor.port.image",
      "action": ["acquisition.memory.port.store", "acquisition.memory.port.return", "control.image"]
    }
  ],
  "working-directory": "/home/robw/GEN2/dzn"
}
