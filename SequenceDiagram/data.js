let Data = {
  "working-directory": "/home/robw/GEN2/dzn",
  "lifelines": [
    {
      "header": {
        "instance": "control",
        "role": "provides"
      },
      "activities": [
        {
          "key": 1,
          "time": 0,
          "location": {
            "file-name": "test/all/Camera/Camera.dzn",
            "line": 57,
            "column": 7
          }
        },
        {
          "key": 28,
          "time": 13,
          "location": {
            "file-name": "test/all/Camera/Camera.dzn",
            "line": 57,
            "column": 7
          }
        },
        {
          "key": 46,
          "time": 22,
          "location": {
            "file-name": "test/all/Camera/Camera.dzn",
            "line": 100,
            "column": 48
          }
        },
        {
          "key": 47,
          "time": 23,
          "location": {
            "file-name": "test/all/Camera/Camera.dzn",
            "line": 70,
            "column": 7
          }
        },
        {
          "key": 66,
          "time": 32,
          "location": {
            "file-name": "test/all/Camera/Camera.dzn",
            "line": 70,
            "column": 7
          }
        },
        {
          "key": 80,
          "time": 39,
          "location": {
            "file-name": "test/all/Camera/Camera.dzn",
            "line": 111,
            "column": 32
          }
        }
      ],
      "labels": [
        {
          "text": "control.setup",
          "role": "provides"
        },
        {
          "text": "control.shoot",
          "role": "provides",
          "illegal": true
        },
        {
          "text": "control.release",
          "role": "provides",
          "illegal": true
        }
      ]
    },
    {
      "header": {
        "instance": "sut.driver",
        "role": "component"
      },
      "activities": [
        {
          "key": 2,
          "time": 0,
          "location": {
            "file-name": "test/all/Camera/Camera.dzn",
            "line": 95,
            "column": 7
          }
        },
        {
          "key": 3,
          "time": 1,
          "location": {
            "file-name": "test/all/Camera/Camera.dzn",
            "line": 95,
            "column": 28
          }
        },
        {
          "key": 18,
          "time": 8,
          "location": {
            "file-name": "test/all/Camera/Optics.dzn",
            "line": 173,
            "column": 5
          }
        },
        {
          "key": 19,
          "time": 9,
          "location": {
            "file-name": "test/all/Camera/Camera.dzn",
            "line": 95,
            "column": 46
          }
        },
        {
          "key": 26,
          "time": 12,
          "location": {
            "file-name": "test/all/Camera/Acquisition.dzn",
            "line": 125,
            "column": 7
          }
        },
        {
          "key": 27,
          "time": 13,
          "location": {
            "file-name": "test/all/Camera/Camera.dzn",
            "line": 95,
            "column": 7
          }
        },
        {
          "key": 42,
          "time": 20,
          "location": {
            "file-name": "test/all/Camera/Optics.dzn",
            "line": 182,
            "column": 63
          }
        },
        {
          "key": 43,
          "time": 21,
          "location": {
            "file-name": "test/all/Camera/Camera.dzn",
            "line": 100,
            "column": 7
          }
        },
        {
          "key": 44,
          "time": 21,
          "location": {
            "file-name": "test/all/Camera/Camera.dzn",
            "line": 100,
            "column": 7
          }
        },
        {
          "key": 45,
          "time": 22,
          "location": {
            "file-name": "test/all/Camera/Camera.dzn",
            "line": 100,
            "column": 48
          }
        },
        {
          "key": 48,
          "time": 23,
          "location": {
            "file-name": "test/all/Camera/Camera.dzn",
            "line": 106,
            "column": 7
          }
        },
        {
          "key": 49,
          "time": 24,
          "location": {
            "file-name": "test/all/Camera/Camera.dzn",
            "line": 106,
            "column": 28
          }
        },
        {
          "key": 56,
          "time": 27,
          "location": {
            "file-name": "test/all/Camera/Acquisition.dzn",
            "line": 133,
            "column": 7
          }
        },
        {
          "key": 57,
          "time": 28,
          "location": {
            "file-name": "test/all/Camera/Camera.dzn",
            "line": 106,
            "column": 51
          }
        },
        {
          "key": 64,
          "time": 31,
          "location": {
            "file-name": "test/all/Camera/Optics.dzn",
            "line": 183,
            "column": 5
          }
        },
        {
          "key": 65,
          "time": 32,
          "location": {
            "file-name": "test/all/Camera/Camera.dzn",
            "line": 106,
            "column": 7
          }
        },
        {
          "key": 76,
          "time": 37,
          "location": {
            "file-name": "test/all/Camera/Acquisition.dzn",
            "line": 137,
            "column": 42
          }
        },
        {
          "key": 77,
          "time": 38,
          "location": {
            "file-name": "test/all/Camera/Camera.dzn",
            "line": 111,
            "column": 7
          }
        },
        {
          "key": 78,
          "time": 38,
          "location": {
            "file-name": "test/all/Camera/Camera.dzn",
            "line": 111,
            "column": 7
          }
        },
        {
          "key": 79,
          "time": 39,
          "location": {
            "file-name": "test/all/Camera/Camera.dzn",
            "line": 111,
            "column": 32
          }
        }
      ],
      "labels": [
        {
          "text": "<back>",
          "role": "component"
        }
      ]
    },
    {
      "header": {
        "instance": "sut.optics.optics",
        "role": "component"
      },
      "activities": [
        {
          "key": 4,
          "time": 1,
          "location": {
            "file-name": "test/all/Camera/Optics.dzn",
            "line": 173,
            "column": 5
          }
        },
        {
          "key": 5,
          "time": 2,
          "location": {
            "file-name": "test/all/Camera/Optics.dzn",
            "line": 175,
            "column": 44
          }
        },
        {
          "key": 16,
          "time": 7,
          "location": {
            "file-name": "test/all/Camera/Optics.dzn",
            "line": 108,
            "column": 12
          }
        },
        {
          "key": 17,
          "time": 8,
          "location": {
            "file-name": "test/all/Camera/Optics.dzn",
            "line": 173,
            "column": 5
          }
        },
        {
          "key": 38,
          "time": 18,
          "location": {
            "file-name": "test/all/Camera/Optics.dzn",
            "line": 112,
            "column": 70
          }
        },
        {
          "key": 39,
          "time": 19,
          "location": {
            "file-name": "test/all/Camera/Optics.dzn",
            "line": 182,
            "column": 21
          }
        },
        {
          "key": 40,
          "time": 19,
          "location": {
            "file-name": "test/all/Camera/Optics.dzn",
            "line": 182,
            "column": 21
          }
        },
        {
          "key": 41,
          "time": 20,
          "location": {
            "file-name": "test/all/Camera/Optics.dzn",
            "line": 182,
            "column": 63
          }
        },
        {
          "key": 58,
          "time": 28,
          "location": {
            "file-name": "test/all/Camera/Optics.dzn",
            "line": 183,
            "column": 5
          }
        },
        {
          "key": 59,
          "time": 29,
          "location": {
            "file-name": "test/all/Camera/Optics.dzn",
            "line": 186,
            "column": 44
          }
        },
        {
          "key": 62,
          "time": 30,
          "location": {
            "file-name": "test/all/Camera/Optics.dzn",
            "line": 125,
            "column": 5
          }
        },
        {
          "key": 63,
          "time": 31,
          "location": {
            "file-name": "test/all/Camera/Optics.dzn",
            "line": 183,
            "column": 5
          }
        }
      ],
      "labels": [
        {
          "text": "<back>",
          "role": "component"
        }
      ]
    },
    {
      "header": {
        "instance": "sut.optics.focus.focus",
        "role": "component"
      },
      "activities": [
        {
          "key": 6,
          "time": 2,
          "location": {
            "file-name": "test/all/Camera/Optics.dzn",
            "line": 108,
            "column": 12
          }
        },
        {
          "key": 7,
          "time": 3,
          "location": {
            "file-name": "test/all/Camera/Optics.dzn",
            "line": 108,
            "column": 61
          }
        },
        {
          "key": 10,
          "time": 4,
          "location": {
            "file-name": "test/all/Camera/Optics.dzn",
            "line": 74,
            "column": 5
          }
        },
        {
          "key": 11,
          "time": 5,
          "location": {
            "file-name": "test/all/Camera/Optics.dzn",
            "line": 108,
            "column": 79
          }
        },
        {
          "key": 14,
          "time": 6,
          "location": {
            "file-name": "test/all/Camera/Optics.dzn",
            "line": 37,
            "column": 7
          }
        },
        {
          "key": 15,
          "time": 7,
          "location": {
            "file-name": "test/all/Camera/Optics.dzn",
            "line": 108,
            "column": 12
          }
        },
        {
          "key": 30,
          "time": 14,
          "location": {
            "file-name": "test/all/Camera/Optics.dzn",
            "line": 43,
            "column": 39
          }
        },
        {
          "key": 31,
          "time": 15,
          "location": {
            "file-name": "test/all/Camera/Optics.dzn",
            "line": 110,
            "column": 13
          }
        },
        {
          "key": 32,
          "time": 15,
          "location": {
            "file-name": "test/all/Camera/Optics.dzn",
            "line": 110,
            "column": 13
          }
        },
        {
          "key": 33,
          "time": 16,
          "location": {
            "file-name": "test/all/Camera/Optics.dzn",
            "line": 111,
            "column": 41
          }
        },
        {
          "key": 36,
          "time": 17,
          "location": {
            "file-name": "test/all/Camera/Optics.dzn",
            "line": 75,
            "column": 5
          }
        },
        {
          "key": 37,
          "time": 18,
          "location": {
            "file-name": "test/all/Camera/Optics.dzn",
            "line": 112,
            "column": 70
          }
        }
      ],
      "labels": [
        {
          "text": "<back>",
          "role": "component"
        }
      ]
    },
    {
      "header": {
        "instance": "sut.optics.focus.lens",
        "role": "foreign"
      },
      "activities": [],
      "labels": [
        {
          "text": "sut.optics.focus.lens.port.stopped",
          "role": "foreign",
          "illegal": true
        }
      ]
    },
    {
      "header": {
        "instance": "sut.optics.focus.lens.port",
        "role": "requires"
      },
      "activities": [
        {
          "key": 12,
          "time": 5,
          "location": {
            "file-name": "test/all/Camera/Optics.dzn",
            "line": 37,
            "column": 7
          }
        },
        {
          "key": 13,
          "time": 6,
          "location": {
            "file-name": "test/all/Camera/Optics.dzn",
            "line": 37,
            "column": 7
          }
        },
        {
          "key": 29,
          "time": 14,
          "location": {
            "file-name": "test/all/Camera/Optics.dzn",
            "line": 43,
            "column": 39
          }
        }
      ],
      "labels": [
        {
          "text": "sut.optics.focus.lens.port.stopped",
          "role": "requires",
          "illegal": true
        }
      ]
    },
    {
      "header": {
        "instance": "sut.optics.focus.sensor",
        "role": "foreign"
      },
      "activities": [],
      "labels": []
    },
    {
      "header": {
        "instance": "sut.optics.focus.sensor.port",
        "role": "requires"
      },
      "activities": [
        {
          "key": 8,
          "time": 3,
          "location": {
            "file-name": "test/all/Camera/Optics.dzn",
            "line": 74,
            "column": 5
          }
        },
        {
          "key": 9,
          "time": 4,
          "location": {
            "file-name": "test/all/Camera/Optics.dzn",
            "line": 74,
            "column": 5
          }
        },
        {
          "key": 34,
          "time": 16,
          "location": {
            "file-name": "test/all/Camera/Optics.dzn",
            "line": 75,
            "column": 5
          }
        },
        {
          "key": 35,
          "time": 17,
          "location": {
            "file-name": "test/all/Camera/Optics.dzn",
            "line": 75,
            "column": 5
          }
        }
      ],
      "labels": []
    },
    {
      "header": {
        "instance": "sut.optics.shutter",
        "role": "foreign"
      },
      "activities": [],
      "labels": []
    },
    {
      "header": {
        "instance": "sut.optics.shutter.port",
        "role": "requires"
      },
      "activities": [
        {
          "key": 60,
          "time": 29,
          "location": {
            "file-name": "test/all/Camera/Optics.dzn",
            "line": 125,
            "column": 5
          }
        },
        {
          "key": 61,
          "time": 30,
          "location": {
            "file-name": "test/all/Camera/Optics.dzn",
            "line": 125,
            "column": 5
          }
        }
      ],
      "labels": []
    },
    {
      "header": {
        "instance": "sut.acquisition.acquire",
        "role": "component"
      },
      "activities": [
        {
          "key": 20,
          "time": 9,
          "location": {
            "file-name": "test/all/Camera/Acquisition.dzn",
            "line": 125,
            "column": 7
          }
        },
        {
          "key": 21,
          "time": 10,
          "location": {
            "file-name": "test/all/Camera/Acquisition.dzn",
            "line": 127,
            "column": 9
          }
        },
        {
          "key": 24,
          "time": 11,
          "location": {
            "file-name": "test/all/Camera/Acquisition.dzn",
            "line": 40,
            "column": 7
          }
        },
        {
          "key": 25,
          "time": 12,
          "location": {
            "file-name": "test/all/Camera/Acquisition.dzn",
            "line": 125,
            "column": 7
          }
        },
        {
          "key": 50,
          "time": 24,
          "location": {
            "file-name": "test/all/Camera/Acquisition.dzn",
            "line": 133,
            "column": 7
          }
        },
        {
          "key": 51,
          "time": 25,
          "location": {
            "file-name": "test/all/Camera/Acquisition.dzn",
            "line": 133,
            "column": 27
          }
        },
        {
          "key": 54,
          "time": 26,
          "location": {
            "file-name": "test/all/Camera/Acquisition.dzn",
            "line": 46,
            "column": 7
          }
        },
        {
          "key": 55,
          "time": 27,
          "location": {
            "file-name": "test/all/Camera/Acquisition.dzn",
            "line": 133,
            "column": 7
          }
        },
        {
          "key": 68,
          "time": 33,
          "location": {
            "file-name": "test/all/Camera/Acquisition.dzn",
            "line": 51,
            "column": 43
          }
        },
        {
          "key": 69,
          "time": 34,
          "location": {
            "file-name": "test/all/Camera/Acquisition.dzn",
            "line": 137,
            "column": 7
          }
        },
        {
          "key": 70,
          "time": 34,
          "location": {
            "file-name": "test/all/Camera/Acquisition.dzn",
            "line": 137,
            "column": 7
          }
        },
        {
          "key": 71,
          "time": 35,
          "location": {
            "file-name": "test/all/Camera/Acquisition.dzn",
            "line": 137,
            "column": 26
          }
        },
        {
          "key": 74,
          "time": 36,
          "location": {
            "file-name": "test/all/Camera/Acquisition.dzn",
            "line": 66,
            "column": 5
          }
        },
        {
          "key": 75,
          "time": 37,
          "location": {
            "file-name": "test/all/Camera/Acquisition.dzn",
            "line": 137,
            "column": 42
          }
        }
      ],
      "labels": [
        {
          "text": "<back>",
          "role": "component"
        }
      ]
    },
    {
      "header": {
        "instance": "sut.acquisition.sensor",
        "role": "foreign"
      },
      "activities": [],
      "labels": [
        {
          "text": "sut.acquisition.sensor.port.image",
          "role": "foreign",
          "illegal": true
        }
      ]
    },
    {
      "header": {
        "instance": "sut.acquisition.sensor.port",
        "role": "requires"
      },
      "activities": [
        {
          "key": 22,
          "time": 10,
          "location": {
            "file-name": "test/all/Camera/Acquisition.dzn",
            "line": 40,
            "column": 7
          }
        },
        {
          "key": 23,
          "time": 11,
          "location": {
            "file-name": "test/all/Camera/Acquisition.dzn",
            "line": 40,
            "column": 7
          }
        },
        {
          "key": 52,
          "time": 25,
          "location": {
            "file-name": "test/all/Camera/Acquisition.dzn",
            "line": 46,
            "column": 7
          }
        },
        {
          "key": 53,
          "time": 26,
          "location": {
            "file-name": "test/all/Camera/Acquisition.dzn",
            "line": 46,
            "column": 7
          }
        },
        {
          "key": 67,
          "time": 33,
          "location": {
            "file-name": "test/all/Camera/Acquisition.dzn",
            "line": 51,
            "column": 43
          }
        }
      ],
      "labels": [
        {
          "text": "sut.acquisition.sensor.port.image",
          "role": "requires",
          "illegal": true
        }
      ]
    },
    {
      "header": {
        "instance": "sut.acquisition.memory",
        "role": "foreign"
      },
      "activities": [],
      "labels": []
    },
    {
      "header": {
        "instance": "sut.acquisition.memory.port",
        "role": "requires"
      },
      "activities": [
        {
          "key": 72,
          "time": 35,
          "location": {
            "file-name": "test/all/Camera/Acquisition.dzn",
            "line": 66,
            "column": 5
          }
        },
        {
          "key": 73,
          "time": 36,
          "location": {
            "file-name": "test/all/Camera/Acquisition.dzn",
            "line": 66,
            "column": 5
          }
        }
      ],
      "labels": []
    }
  ],
  "events": [
    {
      "text": "setup",
      "from": 1,
      "to": 2,
      "type": "in"
    },
    {
      "text": "prepare",
      "from": 3,
      "to": 4,
      "type": "in"
    },
    {
      "text": "measure",
      "from": 5,
      "to": 6,
      "type": "in"
    },
    {
      "text": "measure",
      "from": 7,
      "to": 8,
      "type": "in"
    },
    {
      "text": "EContrast:Sharper",
      "from": 9,
      "to": 10,
      "type": "return"
    },
    {
      "text": "forward",
      "from": 11,
      "to": 12,
      "type": "in"
    },
    {
      "text": "return",
      "from": 13,
      "to": 14,
      "type": "return"
    },
    {
      "text": "return",
      "from": 15,
      "to": 16,
      "type": "return"
    },
    {
      "text": "return",
      "from": 17,
      "to": 18,
      "type": "return"
    },
    {
      "text": "prepare",
      "from": 19,
      "to": 20,
      "type": "in"
    },
    {
      "text": "prepare",
      "from": 21,
      "to": 22,
      "type": "in"
    },
    {
      "text": "return",
      "from": 23,
      "to": 24,
      "type": "return"
    },
    {
      "text": "return",
      "from": 25,
      "to": 26,
      "type": "return"
    },
    {
      "text": "return",
      "from": 27,
      "to": 28,
      "type": "return"
    },
    {
      "text": "stopped",
      "from": 29,
      "to": 30,
      "type": "out"
    },
    {
      "text": "stopped",
      "from": 31,
      "to": 32,
      "type": "out"
    },
    {
      "text": "measure",
      "from": 33,
      "to": 34,
      "type": "in"
    },
    {
      "text": "EContrast:Blurrier",
      "from": 35,
      "to": 36,
      "type": "return"
    },
    {
      "text": "maximum",
      "from": 37,
      "to": 38,
      "type": "out"
    },
    {
      "text": "maximum",
      "from": 39,
      "to": 40,
      "type": "out"
    },
    {
      "text": "ready",
      "from": 41,
      "to": 42,
      "type": "out"
    },
    {
      "text": "ready",
      "from": 43,
      "to": 44,
      "type": "out"
    },
    {
      "text": "focus_lock",
      "from": 45,
      "to": 46,
      "type": "out"
    },
    {
      "text": "shoot",
      "from": 47,
      "to": 48,
      "type": "in"
    },
    {
      "text": "acquire",
      "from": 49,
      "to": 50,
      "type": "in"
    },
    {
      "text": "acquire",
      "from": 51,
      "to": 52,
      "type": "in"
    },
    {
      "text": "return",
      "from": 53,
      "to": 54,
      "type": "return"
    },
    {
      "text": "return",
      "from": 55,
      "to": 56,
      "type": "return"
    },
    {
      "text": "capture",
      "from": 57,
      "to": 58,
      "type": "in"
    },
    {
      "text": "expose",
      "from": 59,
      "to": 60,
      "type": "in"
    },
    {
      "text": "return",
      "from": 61,
      "to": 62,
      "type": "return"
    },
    {
      "text": "return",
      "from": 63,
      "to": 64,
      "type": "return"
    },
    {
      "text": "return",
      "from": 65,
      "to": 66,
      "type": "return"
    },
    {
      "text": "image",
      "from": 67,
      "to": 68,
      "type": "out"
    },
    {
      "text": "image",
      "from": 69,
      "to": 70,
      "type": "out"
    },
    {
      "text": "store",
      "from": 71,
      "to": 72,
      "type": "in"
    },
    {
      "text": "return",
      "from": 73,
      "to": 74,
      "type": "return"
    },
    {
      "text": "image",
      "from": 75,
      "to": 76,
      "type": "out"
    },
    {
      "text": "image",
      "from": 77,
      "to": 78,
      "type": "out"
    },
    {
      "text": "image",
      "from": 79,
      "to": 80,
      "type": "out"
    }
  ],
  "states": [
    [
      {
        "instance": "control",
        "state": [
          {
            "name": "state",
            "value": "State:Idle"
          }
        ]
      },
      {
        "instance": "sut.driver",
        "state": [
          {
            "name": "state",
            "value": "State:Idle"
          }
        ]
      },
      {
        "instance": "sut.optics.optics",
        "state": [
          {
            "name": "state",
            "value": "State:Idle"
          },
          {
            "name": "ready",
            "value": "false"
          }
        ]
      },
      {
        "instance": "sut.optics.focus.focus",
        "state": [
          {
            "name": "idle",
            "value": "true"
          },
          {
            "name": "contrast_was",
            "value": "EContrast:Blurrier"
          }
        ]
      },
      {
        "instance": "sut.optics.focus.lens.port",
        "state": [
          {
            "name": "moving",
            "value": "false"
          }
        ]
      },
      {
        "instance": "sut.optics.focus.sensor.port",
        "state": []
      },
      {
        "instance": "sut.optics.shutter.port",
        "state": []
      },
      {
        "instance": "sut.acquisition.acquire",
        "state": [
          {
            "name": "state",
            "value": "State:Idle"
          }
        ]
      },
      {
        "instance": "sut.acquisition.sensor.port",
        "state": [
          {
            "name": "state",
            "value": "State:Idle"
          }
        ]
      },
      {
        "instance": "sut.acquisition.memory.port",
        "state": []
      }
    ],
    [
      {
        "instance": "control",
        "state": [
          {
            "name": "state",
            "value": "State:Idle"
          }
        ]
      },
      {
        "instance": "sut.driver",
        "state": [
          {
            "name": "state",
            "value": "State:Idle"
          }
        ]
      },
      {
        "instance": "sut.optics.optics",
        "state": [
          {
            "name": "state",
            "value": "State:Idle"
          },
          {
            "name": "ready",
            "value": "false"
          }
        ]
      },
      {
        "instance": "sut.optics.focus.focus",
        "state": [
          {
            "name": "idle",
            "value": "true"
          },
          {
            "name": "contrast_was",
            "value": "EContrast:Blurrier"
          }
        ]
      },
      {
        "instance": "sut.optics.focus.lens.port",
        "state": [
          {
            "name": "moving",
            "value": "false"
          }
        ]
      },
      {
        "instance": "sut.optics.focus.sensor.port",
        "state": []
      },
      {
        "instance": "sut.optics.shutter.port",
        "state": []
      },
      {
        "instance": "sut.acquisition.acquire",
        "state": [
          {
            "name": "state",
            "value": "State:Idle"
          }
        ]
      },
      {
        "instance": "sut.acquisition.sensor.port",
        "state": [
          {
            "name": "state",
            "value": "State:Idle"
          }
        ]
      },
      {
        "instance": "sut.acquisition.memory.port",
        "state": []
      }
    ],
    [
      {
        "instance": "control",
        "state": [
          {
            "name": "state",
            "value": "State:Idle"
          }
        ]
      },
      {
        "instance": "sut.driver",
        "state": [
          {
            "name": "state",
            "value": "State:Idle"
          }
        ]
      },
      {
        "instance": "sut.optics.optics",
        "state": [
          {
            "name": "state",
            "value": "State:Idle"
          },
          {
            "name": "ready",
            "value": "false"
          }
        ]
      },
      {
        "instance": "sut.optics.focus.focus",
        "state": [
          {
            "name": "idle",
            "value": "true"
          },
          {
            "name": "contrast_was",
            "value": "EContrast:Blurrier"
          }
        ]
      },
      {
        "instance": "sut.optics.focus.lens.port",
        "state": [
          {
            "name": "moving",
            "value": "false"
          }
        ]
      },
      {
        "instance": "sut.optics.focus.sensor.port",
        "state": []
      },
      {
        "instance": "sut.optics.shutter.port",
        "state": []
      },
      {
        "instance": "sut.acquisition.acquire",
        "state": [
          {
            "name": "state",
            "value": "State:Idle"
          }
        ]
      },
      {
        "instance": "sut.acquisition.sensor.port",
        "state": [
          {
            "name": "state",
            "value": "State:Idle"
          }
        ]
      },
      {
        "instance": "sut.acquisition.memory.port",
        "state": []
      }
    ],
    [
      {
        "instance": "control",
        "state": [
          {
            "name": "state",
            "value": "State:Idle"
          }
        ]
      },
      {
        "instance": "sut.driver",
        "state": [
          {
            "name": "state",
            "value": "State:Idle"
          }
        ]
      },
      {
        "instance": "sut.optics.optics",
        "state": [
          {
            "name": "state",
            "value": "State:Prepare"
          },
          {
            "name": "ready",
            "value": "false"
          }
        ]
      },
      {
        "instance": "sut.optics.focus.focus",
        "state": [
          {
            "name": "idle",
            "value": "true"
          },
          {
            "name": "contrast_was",
            "value": "EContrast:Blurrier"
          }
        ]
      },
      {
        "instance": "sut.optics.focus.lens.port",
        "state": [
          {
            "name": "moving",
            "value": "false"
          }
        ]
      },
      {
        "instance": "sut.optics.focus.sensor.port",
        "state": []
      },
      {
        "instance": "sut.optics.shutter.port",
        "state": []
      },
      {
        "instance": "sut.acquisition.acquire",
        "state": [
          {
            "name": "state",
            "value": "State:Idle"
          }
        ]
      },
      {
        "instance": "sut.acquisition.sensor.port",
        "state": [
          {
            "name": "state",
            "value": "State:Idle"
          }
        ]
      },
      {
        "instance": "sut.acquisition.memory.port",
        "state": []
      }
    ],
    [
      {
        "instance": "control",
        "state": [
          {
            "name": "state",
            "value": "State:Idle"
          }
        ]
      },
      {
        "instance": "sut.driver",
        "state": [
          {
            "name": "state",
            "value": "State:Idle"
          }
        ]
      },
      {
        "instance": "sut.optics.optics",
        "state": [
          {
            "name": "state",
            "value": "State:Prepare"
          },
          {
            "name": "ready",
            "value": "false"
          }
        ]
      },
      {
        "instance": "sut.optics.focus.focus",
        "state": [
          {
            "name": "idle",
            "value": "false"
          },
          {
            "name": "contrast_was",
            "value": "EContrast:Blurrier"
          }
        ]
      },
      {
        "instance": "sut.optics.focus.lens.port",
        "state": [
          {
            "name": "moving",
            "value": "false"
          }
        ]
      },
      {
        "instance": "sut.optics.focus.sensor.port",
        "state": []
      },
      {
        "instance": "sut.optics.shutter.port",
        "state": []
      },
      {
        "instance": "sut.acquisition.acquire",
        "state": [
          {
            "name": "state",
            "value": "State:Idle"
          }
        ]
      },
      {
        "instance": "sut.acquisition.sensor.port",
        "state": [
          {
            "name": "state",
            "value": "State:Idle"
          }
        ]
      },
      {
        "instance": "sut.acquisition.memory.port",
        "state": []
      }
    ],
    [
      {
        "instance": "control",
        "state": [
          {
            "name": "state",
            "value": "State:Idle"
          }
        ]
      },
      {
        "instance": "sut.driver",
        "state": [
          {
            "name": "state",
            "value": "State:Idle"
          }
        ]
      },
      {
        "instance": "sut.optics.optics",
        "state": [
          {
            "name": "state",
            "value": "State:Prepare"
          },
          {
            "name": "ready",
            "value": "false"
          }
        ]
      },
      {
        "instance": "sut.optics.focus.focus",
        "state": [
          {
            "name": "idle",
            "value": "false"
          },
          {
            "name": "contrast_was",
            "value": "EContrast:Blurrier"
          }
        ]
      },
      {
        "instance": "sut.optics.focus.lens.port",
        "state": [
          {
            "name": "moving",
            "value": "false"
          }
        ]
      },
      {
        "instance": "sut.optics.focus.sensor.port",
        "state": []
      },
      {
        "instance": "sut.optics.shutter.port",
        "state": []
      },
      {
        "instance": "sut.acquisition.acquire",
        "state": [
          {
            "name": "state",
            "value": "State:Idle"
          }
        ]
      },
      {
        "instance": "sut.acquisition.sensor.port",
        "state": [
          {
            "name": "state",
            "value": "State:Idle"
          }
        ]
      },
      {
        "instance": "sut.acquisition.memory.port",
        "state": []
      }
    ],
    [
      {
        "instance": "control",
        "state": [
          {
            "name": "state",
            "value": "State:Idle"
          }
        ]
      },
      {
        "instance": "sut.driver",
        "state": [
          {
            "name": "state",
            "value": "State:Idle"
          }
        ]
      },
      {
        "instance": "sut.optics.optics",
        "state": [
          {
            "name": "state",
            "value": "State:Prepare"
          },
          {
            "name": "ready",
            "value": "false"
          }
        ]
      },
      {
        "instance": "sut.optics.focus.focus",
        "state": [
          {
            "name": "idle",
            "value": "false"
          },
          {
            "name": "contrast_was",
            "value": "EContrast:Sharper"
          }
        ]
      },
      {
        "instance": "sut.optics.focus.lens.port",
        "state": [
          {
            "name": "moving",
            "value": "false"
          }
        ]
      },
      {
        "instance": "sut.optics.focus.sensor.port",
        "state": []
      },
      {
        "instance": "sut.optics.shutter.port",
        "state": []
      },
      {
        "instance": "sut.acquisition.acquire",
        "state": [
          {
            "name": "state",
            "value": "State:Idle"
          }
        ]
      },
      {
        "instance": "sut.acquisition.sensor.port",
        "state": [
          {
            "name": "state",
            "value": "State:Idle"
          }
        ]
      },
      {
        "instance": "sut.acquisition.memory.port",
        "state": []
      }
    ],
    [
      {
        "instance": "control",
        "state": [
          {
            "name": "state",
            "value": "State:Idle"
          }
        ]
      },
      {
        "instance": "sut.driver",
        "state": [
          {
            "name": "state",
            "value": "State:Idle"
          }
        ]
      },
      {
        "instance": "sut.optics.optics",
        "state": [
          {
            "name": "state",
            "value": "State:Prepare"
          },
          {
            "name": "ready",
            "value": "false"
          }
        ]
      },
      {
        "instance": "sut.optics.focus.focus",
        "state": [
          {
            "name": "idle",
            "value": "false"
          },
          {
            "name": "contrast_was",
            "value": "EContrast:Sharper"
          }
        ]
      },
      {
        "instance": "sut.optics.focus.lens.port",
        "state": [
          {
            "name": "moving",
            "value": "true"
          }
        ]
      },
      {
        "instance": "sut.optics.focus.sensor.port",
        "state": []
      },
      {
        "instance": "sut.optics.shutter.port",
        "state": []
      },
      {
        "instance": "sut.acquisition.acquire",
        "state": [
          {
            "name": "state",
            "value": "State:Idle"
          }
        ]
      },
      {
        "instance": "sut.acquisition.sensor.port",
        "state": [
          {
            "name": "state",
            "value": "State:Idle"
          }
        ]
      },
      {
        "instance": "sut.acquisition.memory.port",
        "state": []
      }
    ],
    [
      {
        "instance": "control",
        "state": [
          {
            "name": "state",
            "value": "State:Idle"
          }
        ]
      },
      {
        "instance": "sut.driver",
        "state": [
          {
            "name": "state",
            "value": "State:Idle"
          }
        ]
      },
      {
        "instance": "sut.optics.optics",
        "state": [
          {
            "name": "state",
            "value": "State:Prepare"
          },
          {
            "name": "ready",
            "value": "false"
          }
        ]
      },
      {
        "instance": "sut.optics.focus.focus",
        "state": [
          {
            "name": "idle",
            "value": "false"
          },
          {
            "name": "contrast_was",
            "value": "EContrast:Sharper"
          }
        ]
      },
      {
        "instance": "sut.optics.focus.lens.port",
        "state": [
          {
            "name": "moving",
            "value": "true"
          }
        ]
      },
      {
        "instance": "sut.optics.focus.sensor.port",
        "state": []
      },
      {
        "instance": "sut.optics.shutter.port",
        "state": []
      },
      {
        "instance": "sut.acquisition.acquire",
        "state": [
          {
            "name": "state",
            "value": "State:Idle"
          }
        ]
      },
      {
        "instance": "sut.acquisition.sensor.port",
        "state": [
          {
            "name": "state",
            "value": "State:Idle"
          }
        ]
      },
      {
        "instance": "sut.acquisition.memory.port",
        "state": []
      }
    ],
    [
      {
        "instance": "control",
        "state": [
          {
            "name": "state",
            "value": "State:Idle"
          }
        ]
      },
      {
        "instance": "sut.driver",
        "state": [
          {
            "name": "state",
            "value": "State:Idle"
          }
        ]
      },
      {
        "instance": "sut.optics.optics",
        "state": [
          {
            "name": "state",
            "value": "State:Prepare"
          },
          {
            "name": "ready",
            "value": "false"
          }
        ]
      },
      {
        "instance": "sut.optics.focus.focus",
        "state": [
          {
            "name": "idle",
            "value": "false"
          },
          {
            "name": "contrast_was",
            "value": "EContrast:Sharper"
          }
        ]
      },
      {
        "instance": "sut.optics.focus.lens.port",
        "state": [
          {
            "name": "moving",
            "value": "true"
          }
        ]
      },
      {
        "instance": "sut.optics.focus.sensor.port",
        "state": []
      },
      {
        "instance": "sut.optics.shutter.port",
        "state": []
      },
      {
        "instance": "sut.acquisition.acquire",
        "state": [
          {
            "name": "state",
            "value": "State:Idle"
          }
        ]
      },
      {
        "instance": "sut.acquisition.sensor.port",
        "state": [
          {
            "name": "state",
            "value": "State:Idle"
          }
        ]
      },
      {
        "instance": "sut.acquisition.memory.port",
        "state": []
      }
    ],
    [
      {
        "instance": "control",
        "state": [
          {
            "name": "state",
            "value": "State:Idle"
          }
        ]
      },
      {
        "instance": "sut.driver",
        "state": [
          {
            "name": "state",
            "value": "State:Idle"
          }
        ]
      },
      {
        "instance": "sut.optics.optics",
        "state": [
          {
            "name": "state",
            "value": "State:Prepare"
          },
          {
            "name": "ready",
            "value": "false"
          }
        ]
      },
      {
        "instance": "sut.optics.focus.focus",
        "state": [
          {
            "name": "idle",
            "value": "false"
          },
          {
            "name": "contrast_was",
            "value": "EContrast:Sharper"
          }
        ]
      },
      {
        "instance": "sut.optics.focus.lens.port",
        "state": [
          {
            "name": "moving",
            "value": "true"
          }
        ]
      },
      {
        "instance": "sut.optics.focus.sensor.port",
        "state": []
      },
      {
        "instance": "sut.optics.shutter.port",
        "state": []
      },
      {
        "instance": "sut.acquisition.acquire",
        "state": [
          {
            "name": "state",
            "value": "State:Idle"
          }
        ]
      },
      {
        "instance": "sut.acquisition.sensor.port",
        "state": [
          {
            "name": "state",
            "value": "State:Idle"
          }
        ]
      },
      {
        "instance": "sut.acquisition.memory.port",
        "state": []
      }
    ],
    [
      {
        "instance": "control",
        "state": [
          {
            "name": "state",
            "value": "State:Idle"
          }
        ]
      },
      {
        "instance": "sut.driver",
        "state": [
          {
            "name": "state",
            "value": "State:Idle"
          }
        ]
      },
      {
        "instance": "sut.optics.optics",
        "state": [
          {
            "name": "state",
            "value": "State:Prepare"
          },
          {
            "name": "ready",
            "value": "false"
          }
        ]
      },
      {
        "instance": "sut.optics.focus.focus",
        "state": [
          {
            "name": "idle",
            "value": "false"
          },
          {
            "name": "contrast_was",
            "value": "EContrast:Sharper"
          }
        ]
      },
      {
        "instance": "sut.optics.focus.lens.port",
        "state": [
          {
            "name": "moving",
            "value": "true"
          }
        ]
      },
      {
        "instance": "sut.optics.focus.sensor.port",
        "state": []
      },
      {
        "instance": "sut.optics.shutter.port",
        "state": []
      },
      {
        "instance": "sut.acquisition.acquire",
        "state": [
          {
            "name": "state",
            "value": "State:Idle"
          }
        ]
      },
      {
        "instance": "sut.acquisition.sensor.port",
        "state": [
          {
            "name": "state",
            "value": "State:Idle"
          }
        ]
      },
      {
        "instance": "sut.acquisition.memory.port",
        "state": []
      }
    ],
    [
      {
        "instance": "control",
        "state": [
          {
            "name": "state",
            "value": "State:Idle"
          }
        ]
      },
      {
        "instance": "sut.driver",
        "state": [
          {
            "name": "state",
            "value": "State:Idle"
          }
        ]
      },
      {
        "instance": "sut.optics.optics",
        "state": [
          {
            "name": "state",
            "value": "State:Prepare"
          },
          {
            "name": "ready",
            "value": "false"
          }
        ]
      },
      {
        "instance": "sut.optics.focus.focus",
        "state": [
          {
            "name": "idle",
            "value": "false"
          },
          {
            "name": "contrast_was",
            "value": "EContrast:Sharper"
          }
        ]
      },
      {
        "instance": "sut.optics.focus.lens.port",
        "state": [
          {
            "name": "moving",
            "value": "true"
          }
        ]
      },
      {
        "instance": "sut.optics.focus.sensor.port",
        "state": []
      },
      {
        "instance": "sut.optics.shutter.port",
        "state": []
      },
      {
        "instance": "sut.acquisition.acquire",
        "state": [
          {
            "name": "state",
            "value": "State:Idle"
          }
        ]
      },
      {
        "instance": "sut.acquisition.sensor.port",
        "state": [
          {
            "name": "state",
            "value": "State:Ready"
          }
        ]
      },
      {
        "instance": "sut.acquisition.memory.port",
        "state": []
      }
    ],
    [
      {
        "instance": "control",
        "state": [
          {
            "name": "state",
            "value": "State:Idle"
          }
        ]
      },
      {
        "instance": "sut.driver",
        "state": [
          {
            "name": "state",
            "value": "State:Idle"
          }
        ]
      },
      {
        "instance": "sut.optics.optics",
        "state": [
          {
            "name": "state",
            "value": "State:Prepare"
          },
          {
            "name": "ready",
            "value": "false"
          }
        ]
      },
      {
        "instance": "sut.optics.focus.focus",
        "state": [
          {
            "name": "idle",
            "value": "false"
          },
          {
            "name": "contrast_was",
            "value": "EContrast:Sharper"
          }
        ]
      },
      {
        "instance": "sut.optics.focus.lens.port",
        "state": [
          {
            "name": "moving",
            "value": "true"
          }
        ]
      },
      {
        "instance": "sut.optics.focus.sensor.port",
        "state": []
      },
      {
        "instance": "sut.optics.shutter.port",
        "state": []
      },
      {
        "instance": "sut.acquisition.acquire",
        "state": [
          {
            "name": "state",
            "value": "State:Ready"
          }
        ]
      },
      {
        "instance": "sut.acquisition.sensor.port",
        "state": [
          {
            "name": "state",
            "value": "State:Ready"
          }
        ]
      },
      {
        "instance": "sut.acquisition.memory.port",
        "state": []
      }
    ],
    [
      {
        "instance": "control",
        "state": [
          {
            "name": "state",
            "value": "State:Setup"
          }
        ]
      },
      {
        "instance": "sut.driver",
        "state": [
          {
            "name": "state",
            "value": "State:Setup"
          }
        ]
      },
      {
        "instance": "sut.optics.optics",
        "state": [
          {
            "name": "state",
            "value": "State:Prepare"
          },
          {
            "name": "ready",
            "value": "false"
          }
        ]
      },
      {
        "instance": "sut.optics.focus.focus",
        "state": [
          {
            "name": "idle",
            "value": "false"
          },
          {
            "name": "contrast_was",
            "value": "EContrast:Sharper"
          }
        ]
      },
      {
        "instance": "sut.optics.focus.lens.port",
        "state": [
          {
            "name": "moving",
            "value": "true"
          }
        ]
      },
      {
        "instance": "sut.optics.focus.sensor.port",
        "state": []
      },
      {
        "instance": "sut.optics.shutter.port",
        "state": []
      },
      {
        "instance": "sut.acquisition.acquire",
        "state": [
          {
            "name": "state",
            "value": "State:Ready"
          }
        ]
      },
      {
        "instance": "sut.acquisition.sensor.port",
        "state": [
          {
            "name": "state",
            "value": "State:Ready"
          }
        ]
      },
      {
        "instance": "sut.acquisition.memory.port",
        "state": []
      }
    ],
    [
      {
        "instance": "control",
        "state": [
          {
            "name": "state",
            "value": "State:Setup"
          }
        ]
      },
      {
        "instance": "sut.driver",
        "state": [
          {
            "name": "state",
            "value": "State:Setup"
          }
        ]
      },
      {
        "instance": "sut.optics.optics",
        "state": [
          {
            "name": "state",
            "value": "State:Prepare"
          },
          {
            "name": "ready",
            "value": "false"
          }
        ]
      },
      {
        "instance": "sut.optics.focus.focus",
        "state": [
          {
            "name": "idle",
            "value": "false"
          },
          {
            "name": "contrast_was",
            "value": "EContrast:Sharper"
          }
        ]
      },
      {
        "instance": "sut.optics.focus.lens.port",
        "state": [
          {
            "name": "moving",
            "value": "false"
          }
        ]
      },
      {
        "instance": "sut.optics.focus.sensor.port",
        "state": []
      },
      {
        "instance": "sut.optics.shutter.port",
        "state": []
      },
      {
        "instance": "sut.acquisition.acquire",
        "state": [
          {
            "name": "state",
            "value": "State:Ready"
          }
        ]
      },
      {
        "instance": "sut.acquisition.sensor.port",
        "state": [
          {
            "name": "state",
            "value": "State:Ready"
          }
        ]
      },
      {
        "instance": "sut.acquisition.memory.port",
        "state": []
      }
    ],
    [
      {
        "instance": "control",
        "state": [
          {
            "name": "state",
            "value": "State:Setup"
          }
        ]
      },
      {
        "instance": "sut.driver",
        "state": [
          {
            "name": "state",
            "value": "State:Setup"
          }
        ]
      },
      {
        "instance": "sut.optics.optics",
        "state": [
          {
            "name": "state",
            "value": "State:Prepare"
          },
          {
            "name": "ready",
            "value": "false"
          }
        ]
      },
      {
        "instance": "sut.optics.focus.focus",
        "state": [
          {
            "name": "idle",
            "value": "false"
          },
          {
            "name": "contrast_was",
            "value": "EContrast:Sharper"
          }
        ]
      },
      {
        "instance": "sut.optics.focus.lens.port",
        "state": [
          {
            "name": "moving",
            "value": "false"
          }
        ]
      },
      {
        "instance": "sut.optics.focus.sensor.port",
        "state": []
      },
      {
        "instance": "sut.optics.shutter.port",
        "state": []
      },
      {
        "instance": "sut.acquisition.acquire",
        "state": [
          {
            "name": "state",
            "value": "State:Ready"
          }
        ]
      },
      {
        "instance": "sut.acquisition.sensor.port",
        "state": [
          {
            "name": "state",
            "value": "State:Ready"
          }
        ]
      },
      {
        "instance": "sut.acquisition.memory.port",
        "state": []
      }
    ],
    [
      {
        "instance": "control",
        "state": [
          {
            "name": "state",
            "value": "State:Setup"
          }
        ]
      },
      {
        "instance": "sut.driver",
        "state": [
          {
            "name": "state",
            "value": "State:Setup"
          }
        ]
      },
      {
        "instance": "sut.optics.optics",
        "state": [
          {
            "name": "state",
            "value": "State:Prepare"
          },
          {
            "name": "ready",
            "value": "false"
          }
        ]
      },
      {
        "instance": "sut.optics.focus.focus",
        "state": [
          {
            "name": "idle",
            "value": "false"
          },
          {
            "name": "contrast_was",
            "value": "EContrast:Sharper"
          }
        ]
      },
      {
        "instance": "sut.optics.focus.lens.port",
        "state": [
          {
            "name": "moving",
            "value": "false"
          }
        ]
      },
      {
        "instance": "sut.optics.focus.sensor.port",
        "state": []
      },
      {
        "instance": "sut.optics.shutter.port",
        "state": []
      },
      {
        "instance": "sut.acquisition.acquire",
        "state": [
          {
            "name": "state",
            "value": "State:Ready"
          }
        ]
      },
      {
        "instance": "sut.acquisition.sensor.port",
        "state": [
          {
            "name": "state",
            "value": "State:Ready"
          }
        ]
      },
      {
        "instance": "sut.acquisition.memory.port",
        "state": []
      }
    ],
    [
      {
        "instance": "control",
        "state": [
          {
            "name": "state",
            "value": "State:Setup"
          }
        ]
      },
      {
        "instance": "sut.driver",
        "state": [
          {
            "name": "state",
            "value": "State:Setup"
          }
        ]
      },
      {
        "instance": "sut.optics.optics",
        "state": [
          {
            "name": "state",
            "value": "State:Prepare"
          },
          {
            "name": "ready",
            "value": "false"
          }
        ]
      },
      {
        "instance": "sut.optics.focus.focus",
        "state": [
          {
            "name": "idle",
            "value": "false"
          },
          {
            "name": "contrast_was",
            "value": "EContrast:Sharper"
          }
        ]
      },
      {
        "instance": "sut.optics.focus.lens.port",
        "state": [
          {
            "name": "moving",
            "value": "false"
          }
        ]
      },
      {
        "instance": "sut.optics.focus.sensor.port",
        "state": []
      },
      {
        "instance": "sut.optics.shutter.port",
        "state": []
      },
      {
        "instance": "sut.acquisition.acquire",
        "state": [
          {
            "name": "state",
            "value": "State:Ready"
          }
        ]
      },
      {
        "instance": "sut.acquisition.sensor.port",
        "state": [
          {
            "name": "state",
            "value": "State:Ready"
          }
        ]
      },
      {
        "instance": "sut.acquisition.memory.port",
        "state": []
      }
    ],
    [
      {
        "instance": "control",
        "state": [
          {
            "name": "state",
            "value": "State:Setup"
          }
        ]
      },
      {
        "instance": "sut.driver",
        "state": [
          {
            "name": "state",
            "value": "State:Setup"
          }
        ]
      },
      {
        "instance": "sut.optics.optics",
        "state": [
          {
            "name": "state",
            "value": "State:Prepare"
          },
          {
            "name": "ready",
            "value": "false"
          }
        ]
      },
      {
        "instance": "sut.optics.focus.focus",
        "state": [
          {
            "name": "contrast_is",
            "value": "EContrast:Blurrier"
          },
          {
            "name": "idle",
            "value": "true"
          },
          {
            "name": "contrast_was",
            "value": "EContrast:Sharper"
          }
        ]
      },
      {
        "instance": "sut.optics.focus.lens.port",
        "state": [
          {
            "name": "moving",
            "value": "false"
          }
        ]
      },
      {
        "instance": "sut.optics.focus.sensor.port",
        "state": []
      },
      {
        "instance": "sut.optics.shutter.port",
        "state": []
      },
      {
        "instance": "sut.acquisition.acquire",
        "state": [
          {
            "name": "state",
            "value": "State:Ready"
          }
        ]
      },
      {
        "instance": "sut.acquisition.sensor.port",
        "state": [
          {
            "name": "state",
            "value": "State:Ready"
          }
        ]
      },
      {
        "instance": "sut.acquisition.memory.port",
        "state": []
      }
    ],
    [
      {
        "instance": "control",
        "state": [
          {
            "name": "state",
            "value": "State:Setup"
          }
        ]
      },
      {
        "instance": "sut.driver",
        "state": [
          {
            "name": "state",
            "value": "State:Setup"
          }
        ]
      },
      {
        "instance": "sut.optics.optics",
        "state": [
          {
            "name": "state",
            "value": "State:Prepare"
          },
          {
            "name": "ready",
            "value": "false"
          }
        ]
      },
      {
        "instance": "sut.optics.focus.focus",
        "state": [
          {
            "name": "contrast_is",
            "value": "EContrast:Blurrier"
          },
          {
            "name": "idle",
            "value": "true"
          },
          {
            "name": "contrast_was",
            "value": "EContrast:Blurrier"
          }
        ]
      },
      {
        "instance": "sut.optics.focus.lens.port",
        "state": [
          {
            "name": "moving",
            "value": "false"
          }
        ]
      },
      {
        "instance": "sut.optics.focus.sensor.port",
        "state": []
      },
      {
        "instance": "sut.optics.shutter.port",
        "state": []
      },
      {
        "instance": "sut.acquisition.acquire",
        "state": [
          {
            "name": "state",
            "value": "State:Ready"
          }
        ]
      },
      {
        "instance": "sut.acquisition.sensor.port",
        "state": [
          {
            "name": "state",
            "value": "State:Ready"
          }
        ]
      },
      {
        "instance": "sut.acquisition.memory.port",
        "state": []
      }
    ],
    [
      {
        "instance": "control",
        "state": [
          {
            "name": "state",
            "value": "State:Setup"
          }
        ]
      },
      {
        "instance": "sut.driver",
        "state": [
          {
            "name": "state",
            "value": "State:Setup"
          }
        ]
      },
      {
        "instance": "sut.optics.optics",
        "state": [
          {
            "name": "state",
            "value": "State:Ready"
          },
          {
            "name": "ready",
            "value": "false"
          }
        ]
      },
      {
        "instance": "sut.optics.focus.focus",
        "state": [
          {
            "name": "contrast_is",
            "value": "EContrast:Blurrier"
          },
          {
            "name": "idle",
            "value": "true"
          },
          {
            "name": "contrast_was",
            "value": "EContrast:Blurrier"
          }
        ]
      },
      {
        "instance": "sut.optics.focus.lens.port",
        "state": [
          {
            "name": "moving",
            "value": "false"
          }
        ]
      },
      {
        "instance": "sut.optics.focus.sensor.port",
        "state": []
      },
      {
        "instance": "sut.optics.shutter.port",
        "state": []
      },
      {
        "instance": "sut.acquisition.acquire",
        "state": [
          {
            "name": "state",
            "value": "State:Ready"
          }
        ]
      },
      {
        "instance": "sut.acquisition.sensor.port",
        "state": [
          {
            "name": "state",
            "value": "State:Ready"
          }
        ]
      },
      {
        "instance": "sut.acquisition.memory.port",
        "state": []
      }
    ],
    [
      {
        "instance": "control",
        "state": [
          {
            "name": "state",
            "value": "State:Setup"
          }
        ]
      },
      {
        "instance": "sut.driver",
        "state": [
          {
            "name": "state",
            "value": "State:Setup"
          }
        ]
      },
      {
        "instance": "sut.optics.optics",
        "state": [
          {
            "name": "state",
            "value": "State:Ready"
          },
          {
            "name": "ready",
            "value": "false"
          }
        ]
      },
      {
        "instance": "sut.optics.focus.focus",
        "state": [
          {
            "name": "contrast_is",
            "value": "EContrast:Blurrier"
          },
          {
            "name": "idle",
            "value": "true"
          },
          {
            "name": "contrast_was",
            "value": "EContrast:Blurrier"
          }
        ]
      },
      {
        "instance": "sut.optics.focus.lens.port",
        "state": [
          {
            "name": "moving",
            "value": "false"
          }
        ]
      },
      {
        "instance": "sut.optics.focus.sensor.port",
        "state": []
      },
      {
        "instance": "sut.optics.shutter.port",
        "state": []
      },
      {
        "instance": "sut.acquisition.acquire",
        "state": [
          {
            "name": "state",
            "value": "State:Ready"
          }
        ]
      },
      {
        "instance": "sut.acquisition.sensor.port",
        "state": [
          {
            "name": "state",
            "value": "State:Ready"
          }
        ]
      },
      {
        "instance": "sut.acquisition.memory.port",
        "state": []
      }
    ],
    [
      {
        "instance": "control",
        "state": [
          {
            "name": "state",
            "value": "State:Ready"
          }
        ]
      },
      {
        "instance": "sut.driver",
        "state": [
          {
            "name": "state",
            "value": "State:Ready"
          }
        ]
      },
      {
        "instance": "sut.optics.optics",
        "state": [
          {
            "name": "state",
            "value": "State:Ready"
          },
          {
            "name": "ready",
            "value": "false"
          }
        ]
      },
      {
        "instance": "sut.optics.focus.focus",
        "state": [
          {
            "name": "idle",
            "value": "true"
          },
          {
            "name": "contrast_was",
            "value": "EContrast:Blurrier"
          }
        ]
      },
      {
        "instance": "sut.optics.focus.lens.port",
        "state": [
          {
            "name": "moving",
            "value": "false"
          }
        ]
      },
      {
        "instance": "sut.optics.focus.sensor.port",
        "state": []
      },
      {
        "instance": "sut.optics.shutter.port",
        "state": []
      },
      {
        "instance": "sut.acquisition.acquire",
        "state": [
          {
            "name": "state",
            "value": "State:Ready"
          }
        ]
      },
      {
        "instance": "sut.acquisition.sensor.port",
        "state": [
          {
            "name": "state",
            "value": "State:Ready"
          }
        ]
      },
      {
        "instance": "sut.acquisition.memory.port",
        "state": []
      }
    ],
    [
      {
        "instance": "control",
        "state": [
          {
            "name": "state",
            "value": "State:Ready"
          }
        ]
      },
      {
        "instance": "sut.driver",
        "state": [
          {
            "name": "state",
            "value": "State:Ready"
          }
        ]
      },
      {
        "instance": "sut.optics.optics",
        "state": [
          {
            "name": "state",
            "value": "State:Ready"
          },
          {
            "name": "ready",
            "value": "false"
          }
        ]
      },
      {
        "instance": "sut.optics.focus.focus",
        "state": [
          {
            "name": "idle",
            "value": "true"
          },
          {
            "name": "contrast_was",
            "value": "EContrast:Blurrier"
          }
        ]
      },
      {
        "instance": "sut.optics.focus.lens.port",
        "state": [
          {
            "name": "moving",
            "value": "false"
          }
        ]
      },
      {
        "instance": "sut.optics.focus.sensor.port",
        "state": []
      },
      {
        "instance": "sut.optics.shutter.port",
        "state": []
      },
      {
        "instance": "sut.acquisition.acquire",
        "state": [
          {
            "name": "state",
            "value": "State:Ready"
          }
        ]
      },
      {
        "instance": "sut.acquisition.sensor.port",
        "state": [
          {
            "name": "state",
            "value": "State:Ready"
          }
        ]
      },
      {
        "instance": "sut.acquisition.memory.port",
        "state": []
      }
    ],
    [
      {
        "instance": "control",
        "state": [
          {
            "name": "state",
            "value": "State:Ready"
          }
        ]
      },
      {
        "instance": "sut.driver",
        "state": [
          {
            "name": "state",
            "value": "State:Ready"
          }
        ]
      },
      {
        "instance": "sut.optics.optics",
        "state": [
          {
            "name": "state",
            "value": "State:Ready"
          },
          {
            "name": "ready",
            "value": "false"
          }
        ]
      },
      {
        "instance": "sut.optics.focus.focus",
        "state": [
          {
            "name": "idle",
            "value": "true"
          },
          {
            "name": "contrast_was",
            "value": "EContrast:Blurrier"
          }
        ]
      },
      {
        "instance": "sut.optics.focus.lens.port",
        "state": [
          {
            "name": "moving",
            "value": "false"
          }
        ]
      },
      {
        "instance": "sut.optics.focus.sensor.port",
        "state": []
      },
      {
        "instance": "sut.optics.shutter.port",
        "state": []
      },
      {
        "instance": "sut.acquisition.acquire",
        "state": [
          {
            "name": "state",
            "value": "State:Ready"
          }
        ]
      },
      {
        "instance": "sut.acquisition.sensor.port",
        "state": [
          {
            "name": "state",
            "value": "State:Ready"
          }
        ]
      },
      {
        "instance": "sut.acquisition.memory.port",
        "state": []
      }
    ],
    [
      {
        "instance": "control",
        "state": [
          {
            "name": "state",
            "value": "State:Ready"
          }
        ]
      },
      {
        "instance": "sut.driver",
        "state": [
          {
            "name": "state",
            "value": "State:Ready"
          }
        ]
      },
      {
        "instance": "sut.optics.optics",
        "state": [
          {
            "name": "state",
            "value": "State:Ready"
          },
          {
            "name": "ready",
            "value": "false"
          }
        ]
      },
      {
        "instance": "sut.optics.focus.focus",
        "state": [
          {
            "name": "idle",
            "value": "true"
          },
          {
            "name": "contrast_was",
            "value": "EContrast:Blurrier"
          }
        ]
      },
      {
        "instance": "sut.optics.focus.lens.port",
        "state": [
          {
            "name": "moving",
            "value": "false"
          }
        ]
      },
      {
        "instance": "sut.optics.focus.sensor.port",
        "state": []
      },
      {
        "instance": "sut.optics.shutter.port",
        "state": []
      },
      {
        "instance": "sut.acquisition.acquire",
        "state": [
          {
            "name": "state",
            "value": "State:Ready"
          }
        ]
      },
      {
        "instance": "sut.acquisition.sensor.port",
        "state": [
          {
            "name": "state",
            "value": "State:Ready"
          }
        ]
      },
      {
        "instance": "sut.acquisition.memory.port",
        "state": []
      }
    ],
    [
      {
        "instance": "control",
        "state": [
          {
            "name": "state",
            "value": "State:Ready"
          }
        ]
      },
      {
        "instance": "sut.driver",
        "state": [
          {
            "name": "state",
            "value": "State:Ready"
          }
        ]
      },
      {
        "instance": "sut.optics.optics",
        "state": [
          {
            "name": "state",
            "value": "State:Ready"
          },
          {
            "name": "ready",
            "value": "false"
          }
        ]
      },
      {
        "instance": "sut.optics.focus.focus",
        "state": [
          {
            "name": "idle",
            "value": "true"
          },
          {
            "name": "contrast_was",
            "value": "EContrast:Blurrier"
          }
        ]
      },
      {
        "instance": "sut.optics.focus.lens.port",
        "state": [
          {
            "name": "moving",
            "value": "false"
          }
        ]
      },
      {
        "instance": "sut.optics.focus.sensor.port",
        "state": []
      },
      {
        "instance": "sut.optics.shutter.port",
        "state": []
      },
      {
        "instance": "sut.acquisition.acquire",
        "state": [
          {
            "name": "state",
            "value": "State:Ready"
          }
        ]
      },
      {
        "instance": "sut.acquisition.sensor.port",
        "state": [
          {
            "name": "state",
            "value": "State:Acquire"
          }
        ]
      },
      {
        "instance": "sut.acquisition.memory.port",
        "state": []
      }
    ],
    [
      {
        "instance": "control",
        "state": [
          {
            "name": "state",
            "value": "State:Ready"
          }
        ]
      },
      {
        "instance": "sut.driver",
        "state": [
          {
            "name": "state",
            "value": "State:Ready"
          }
        ]
      },
      {
        "instance": "sut.optics.optics",
        "state": [
          {
            "name": "state",
            "value": "State:Ready"
          },
          {
            "name": "ready",
            "value": "false"
          }
        ]
      },
      {
        "instance": "sut.optics.focus.focus",
        "state": [
          {
            "name": "idle",
            "value": "true"
          },
          {
            "name": "contrast_was",
            "value": "EContrast:Blurrier"
          }
        ]
      },
      {
        "instance": "sut.optics.focus.lens.port",
        "state": [
          {
            "name": "moving",
            "value": "false"
          }
        ]
      },
      {
        "instance": "sut.optics.focus.sensor.port",
        "state": []
      },
      {
        "instance": "sut.optics.shutter.port",
        "state": []
      },
      {
        "instance": "sut.acquisition.acquire",
        "state": [
          {
            "name": "state",
            "value": "State:Acquiring"
          }
        ]
      },
      {
        "instance": "sut.acquisition.sensor.port",
        "state": [
          {
            "name": "state",
            "value": "State:Acquire"
          }
        ]
      },
      {
        "instance": "sut.acquisition.memory.port",
        "state": []
      }
    ],
    [
      {
        "instance": "control",
        "state": [
          {
            "name": "state",
            "value": "State:Ready"
          }
        ]
      },
      {
        "instance": "sut.driver",
        "state": [
          {
            "name": "state",
            "value": "State:Ready"
          }
        ]
      },
      {
        "instance": "sut.optics.optics",
        "state": [
          {
            "name": "state",
            "value": "State:Ready"
          },
          {
            "name": "ready",
            "value": "false"
          }
        ]
      },
      {
        "instance": "sut.optics.focus.focus",
        "state": [
          {
            "name": "idle",
            "value": "true"
          },
          {
            "name": "contrast_was",
            "value": "EContrast:Blurrier"
          }
        ]
      },
      {
        "instance": "sut.optics.focus.lens.port",
        "state": [
          {
            "name": "moving",
            "value": "false"
          }
        ]
      },
      {
        "instance": "sut.optics.focus.sensor.port",
        "state": []
      },
      {
        "instance": "sut.optics.shutter.port",
        "state": []
      },
      {
        "instance": "sut.acquisition.acquire",
        "state": [
          {
            "name": "state",
            "value": "State:Acquiring"
          }
        ]
      },
      {
        "instance": "sut.acquisition.sensor.port",
        "state": [
          {
            "name": "state",
            "value": "State:Acquire"
          }
        ]
      },
      {
        "instance": "sut.acquisition.memory.port",
        "state": []
      }
    ],
    [
      {
        "instance": "control",
        "state": [
          {
            "name": "state",
            "value": "State:Ready"
          }
        ]
      },
      {
        "instance": "sut.driver",
        "state": [
          {
            "name": "state",
            "value": "State:Ready"
          }
        ]
      },
      {
        "instance": "sut.optics.optics",
        "state": [
          {
            "name": "state",
            "value": "State:Idle"
          },
          {
            "name": "ready",
            "value": "false"
          }
        ]
      },
      {
        "instance": "sut.optics.focus.focus",
        "state": [
          {
            "name": "idle",
            "value": "true"
          },
          {
            "name": "contrast_was",
            "value": "EContrast:Blurrier"
          }
        ]
      },
      {
        "instance": "sut.optics.focus.lens.port",
        "state": [
          {
            "name": "moving",
            "value": "false"
          }
        ]
      },
      {
        "instance": "sut.optics.focus.sensor.port",
        "state": []
      },
      {
        "instance": "sut.optics.shutter.port",
        "state": []
      },
      {
        "instance": "sut.acquisition.acquire",
        "state": [
          {
            "name": "state",
            "value": "State:Acquiring"
          }
        ]
      },
      {
        "instance": "sut.acquisition.sensor.port",
        "state": [
          {
            "name": "state",
            "value": "State:Acquire"
          }
        ]
      },
      {
        "instance": "sut.acquisition.memory.port",
        "state": []
      }
    ],
    [
      {
        "instance": "control",
        "state": [
          {
            "name": "state",
            "value": "State:Ready"
          }
        ]
      },
      {
        "instance": "sut.driver",
        "state": [
          {
            "name": "state",
            "value": "State:Ready"
          }
        ]
      },
      {
        "instance": "sut.optics.optics",
        "state": [
          {
            "name": "state",
            "value": "State:Idle"
          },
          {
            "name": "ready",
            "value": "false"
          }
        ]
      },
      {
        "instance": "sut.optics.focus.focus",
        "state": [
          {
            "name": "idle",
            "value": "true"
          },
          {
            "name": "contrast_was",
            "value": "EContrast:Blurrier"
          }
        ]
      },
      {
        "instance": "sut.optics.focus.lens.port",
        "state": [
          {
            "name": "moving",
            "value": "false"
          }
        ]
      },
      {
        "instance": "sut.optics.focus.sensor.port",
        "state": []
      },
      {
        "instance": "sut.optics.shutter.port",
        "state": []
      },
      {
        "instance": "sut.acquisition.acquire",
        "state": [
          {
            "name": "state",
            "value": "State:Acquiring"
          }
        ]
      },
      {
        "instance": "sut.acquisition.sensor.port",
        "state": [
          {
            "name": "state",
            "value": "State:Acquire"
          }
        ]
      },
      {
        "instance": "sut.acquisition.memory.port",
        "state": []
      }
    ],
    [
      {
        "instance": "control",
        "state": [
          {
            "name": "state",
            "value": "State:Ready"
          }
        ]
      },
      {
        "instance": "sut.driver",
        "state": [
          {
            "name": "state",
            "value": "State:Ready"
          }
        ]
      },
      {
        "instance": "sut.optics.optics",
        "state": [
          {
            "name": "state",
            "value": "State:Idle"
          },
          {
            "name": "ready",
            "value": "false"
          }
        ]
      },
      {
        "instance": "sut.optics.focus.focus",
        "state": [
          {
            "name": "idle",
            "value": "true"
          },
          {
            "name": "contrast_was",
            "value": "EContrast:Blurrier"
          }
        ]
      },
      {
        "instance": "sut.optics.focus.lens.port",
        "state": [
          {
            "name": "moving",
            "value": "false"
          }
        ]
      },
      {
        "instance": "sut.optics.focus.sensor.port",
        "state": []
      },
      {
        "instance": "sut.optics.shutter.port",
        "state": []
      },
      {
        "instance": "sut.acquisition.acquire",
        "state": [
          {
            "name": "state",
            "value": "State:Acquiring"
          }
        ]
      },
      {
        "instance": "sut.acquisition.sensor.port",
        "state": [
          {
            "name": "state",
            "value": "State:Acquire"
          }
        ]
      },
      {
        "instance": "sut.acquisition.memory.port",
        "state": []
      }
    ],
    [
      {
        "instance": "control",
        "state": [
          {
            "name": "state",
            "value": "State:Acquire"
          }
        ]
      },
      {
        "instance": "sut.driver",
        "state": [
          {
            "name": "state",
            "value": "State:Acquire"
          }
        ]
      },
      {
        "instance": "sut.optics.optics",
        "state": [
          {
            "name": "state",
            "value": "State:Idle"
          },
          {
            "name": "ready",
            "value": "false"
          }
        ]
      },
      {
        "instance": "sut.optics.focus.focus",
        "state": [
          {
            "name": "idle",
            "value": "true"
          },
          {
            "name": "contrast_was",
            "value": "EContrast:Blurrier"
          }
        ]
      },
      {
        "instance": "sut.optics.focus.lens.port",
        "state": [
          {
            "name": "moving",
            "value": "false"
          }
        ]
      },
      {
        "instance": "sut.optics.focus.sensor.port",
        "state": []
      },
      {
        "instance": "sut.optics.shutter.port",
        "state": []
      },
      {
        "instance": "sut.acquisition.acquire",
        "state": [
          {
            "name": "state",
            "value": "State:Acquiring"
          }
        ]
      },
      {
        "instance": "sut.acquisition.sensor.port",
        "state": [
          {
            "name": "state",
            "value": "State:Acquire"
          }
        ]
      },
      {
        "instance": "sut.acquisition.memory.port",
        "state": []
      }
    ],
    [
      {
        "instance": "control",
        "state": [
          {
            "name": "state",
            "value": "State:Acquire"
          }
        ]
      },
      {
        "instance": "sut.driver",
        "state": [
          {
            "name": "state",
            "value": "State:Acquire"
          }
        ]
      },
      {
        "instance": "sut.optics.optics",
        "state": [
          {
            "name": "state",
            "value": "State:Idle"
          },
          {
            "name": "ready",
            "value": "false"
          }
        ]
      },
      {
        "instance": "sut.optics.focus.focus",
        "state": [
          {
            "name": "idle",
            "value": "true"
          },
          {
            "name": "contrast_was",
            "value": "EContrast:Blurrier"
          }
        ]
      },
      {
        "instance": "sut.optics.focus.lens.port",
        "state": [
          {
            "name": "moving",
            "value": "false"
          }
        ]
      },
      {
        "instance": "sut.optics.focus.sensor.port",
        "state": []
      },
      {
        "instance": "sut.optics.shutter.port",
        "state": []
      },
      {
        "instance": "sut.acquisition.acquire",
        "state": [
          {
            "name": "state",
            "value": "State:Acquiring"
          }
        ]
      },
      {
        "instance": "sut.acquisition.sensor.port",
        "state": [
          {
            "name": "state",
            "value": "State:Idle"
          }
        ]
      },
      {
        "instance": "sut.acquisition.memory.port",
        "state": []
      }
    ],
    [
      {
        "instance": "control",
        "state": [
          {
            "name": "state",
            "value": "State:Acquire"
          }
        ]
      },
      {
        "instance": "sut.driver",
        "state": [
          {
            "name": "state",
            "value": "State:Acquire"
          }
        ]
      },
      {
        "instance": "sut.optics.optics",
        "state": [
          {
            "name": "state",
            "value": "State:Idle"
          },
          {
            "name": "ready",
            "value": "false"
          }
        ]
      },
      {
        "instance": "sut.optics.focus.focus",
        "state": [
          {
            "name": "idle",
            "value": "true"
          },
          {
            "name": "contrast_was",
            "value": "EContrast:Blurrier"
          }
        ]
      },
      {
        "instance": "sut.optics.focus.lens.port",
        "state": [
          {
            "name": "moving",
            "value": "false"
          }
        ]
      },
      {
        "instance": "sut.optics.focus.sensor.port",
        "state": []
      },
      {
        "instance": "sut.optics.shutter.port",
        "state": []
      },
      {
        "instance": "sut.acquisition.acquire",
        "state": [
          {
            "name": "state",
            "value": "State:Acquiring"
          }
        ]
      },
      {
        "instance": "sut.acquisition.sensor.port",
        "state": [
          {
            "name": "state",
            "value": "State:Idle"
          }
        ]
      },
      {
        "instance": "sut.acquisition.memory.port",
        "state": []
      }
    ],
    [
      {
        "instance": "control",
        "state": [
          {
            "name": "state",
            "value": "State:Acquire"
          }
        ]
      },
      {
        "instance": "sut.driver",
        "state": [
          {
            "name": "state",
            "value": "State:Acquire"
          }
        ]
      },
      {
        "instance": "sut.optics.optics",
        "state": [
          {
            "name": "state",
            "value": "State:Idle"
          },
          {
            "name": "ready",
            "value": "false"
          }
        ]
      },
      {
        "instance": "sut.optics.focus.focus",
        "state": [
          {
            "name": "idle",
            "value": "true"
          },
          {
            "name": "contrast_was",
            "value": "EContrast:Blurrier"
          }
        ]
      },
      {
        "instance": "sut.optics.focus.lens.port",
        "state": [
          {
            "name": "moving",
            "value": "false"
          }
        ]
      },
      {
        "instance": "sut.optics.focus.sensor.port",
        "state": []
      },
      {
        "instance": "sut.optics.shutter.port",
        "state": []
      },
      {
        "instance": "sut.acquisition.acquire",
        "state": [
          {
            "name": "state",
            "value": "State:Acquiring"
          }
        ]
      },
      {
        "instance": "sut.acquisition.sensor.port",
        "state": [
          {
            "name": "state",
            "value": "State:Idle"
          }
        ]
      },
      {
        "instance": "sut.acquisition.memory.port",
        "state": []
      }
    ],
    [
      {
        "instance": "control",
        "state": [
          {
            "name": "state",
            "value": "State:Acquire"
          }
        ]
      },
      {
        "instance": "sut.driver",
        "state": [
          {
            "name": "state",
            "value": "State:Acquire"
          }
        ]
      },
      {
        "instance": "sut.optics.optics",
        "state": [
          {
            "name": "state",
            "value": "State:Idle"
          },
          {
            "name": "ready",
            "value": "false"
          }
        ]
      },
      {
        "instance": "sut.optics.focus.focus",
        "state": [
          {
            "name": "idle",
            "value": "true"
          },
          {
            "name": "contrast_was",
            "value": "EContrast:Blurrier"
          }
        ]
      },
      {
        "instance": "sut.optics.focus.lens.port",
        "state": [
          {
            "name": "moving",
            "value": "false"
          }
        ]
      },
      {
        "instance": "sut.optics.focus.sensor.port",
        "state": []
      },
      {
        "instance": "sut.optics.shutter.port",
        "state": []
      },
      {
        "instance": "sut.acquisition.acquire",
        "state": [
          {
            "name": "state",
            "value": "State:Acquiring"
          }
        ]
      },
      {
        "instance": "sut.acquisition.sensor.port",
        "state": [
          {
            "name": "state",
            "value": "State:Idle"
          }
        ]
      },
      {
        "instance": "sut.acquisition.memory.port",
        "state": []
      }
    ],
    [
      {
        "instance": "control",
        "state": [
          {
            "name": "state",
            "value": "State:Acquire"
          }
        ]
      },
      {
        "instance": "sut.driver",
        "state": [
          {
            "name": "state",
            "value": "State:Acquire"
          }
        ]
      },
      {
        "instance": "sut.optics.optics",
        "state": [
          {
            "name": "state",
            "value": "State:Idle"
          },
          {
            "name": "ready",
            "value": "false"
          }
        ]
      },
      {
        "instance": "sut.optics.focus.focus",
        "state": [
          {
            "name": "idle",
            "value": "true"
          },
          {
            "name": "contrast_was",
            "value": "EContrast:Blurrier"
          }
        ]
      },
      {
        "instance": "sut.optics.focus.lens.port",
        "state": [
          {
            "name": "moving",
            "value": "false"
          }
        ]
      },
      {
        "instance": "sut.optics.focus.sensor.port",
        "state": []
      },
      {
        "instance": "sut.optics.shutter.port",
        "state": []
      },
      {
        "instance": "sut.acquisition.acquire",
        "state": [
          {
            "name": "state",
            "value": "State:Acquiring"
          }
        ]
      },
      {
        "instance": "sut.acquisition.sensor.port",
        "state": [
          {
            "name": "state",
            "value": "State:Idle"
          }
        ]
      },
      {
        "instance": "sut.acquisition.memory.port",
        "state": []
      }
    ],
    [
      {
        "instance": "control",
        "state": [
          {
            "name": "state",
            "value": "State:Acquire"
          }
        ]
      },
      {
        "instance": "sut.driver",
        "state": [
          {
            "name": "state",
            "value": "State:Acquire"
          }
        ]
      },
      {
        "instance": "sut.optics.optics",
        "state": [
          {
            "name": "state",
            "value": "State:Idle"
          },
          {
            "name": "ready",
            "value": "false"
          }
        ]
      },
      {
        "instance": "sut.optics.focus.focus",
        "state": [
          {
            "name": "idle",
            "value": "true"
          },
          {
            "name": "contrast_was",
            "value": "EContrast:Blurrier"
          }
        ]
      },
      {
        "instance": "sut.optics.focus.lens.port",
        "state": [
          {
            "name": "moving",
            "value": "false"
          }
        ]
      },
      {
        "instance": "sut.optics.focus.sensor.port",
        "state": []
      },
      {
        "instance": "sut.optics.shutter.port",
        "state": []
      },
      {
        "instance": "sut.acquisition.acquire",
        "state": [
          {
            "name": "state",
            "value": "State:Idle"
          }
        ]
      },
      {
        "instance": "sut.acquisition.sensor.port",
        "state": [
          {
            "name": "state",
            "value": "State:Idle"
          }
        ]
      },
      {
        "instance": "sut.acquisition.memory.port",
        "state": []
      }
    ],
    [
      {
        "instance": "control",
        "state": [
          {
            "name": "state",
            "value": "State:Idle"
          }
        ]
      },
      {
        "instance": "sut.driver",
        "state": [
          {
            "name": "state",
            "value": "State:Idle"
          }
        ]
      },
      {
        "instance": "sut.optics.optics",
        "state": [
          {
            "name": "state",
            "value": "State:Idle"
          },
          {
            "name": "ready",
            "value": "false"
          }
        ]
      },
      {
        "instance": "sut.optics.focus.focus",
        "state": [
          {
            "name": "idle",
            "value": "true"
          },
          {
            "name": "contrast_was",
            "value": "EContrast:Blurrier"
          }
        ]
      },
      {
        "instance": "sut.optics.focus.lens.port",
        "state": [
          {
            "name": "moving",
            "value": "false"
          }
        ]
      },
      {
        "instance": "sut.optics.focus.sensor.port",
        "state": []
      },
      {
        "instance": "sut.optics.shutter.port",
        "state": []
      },
      {
        "instance": "sut.acquisition.acquire",
        "state": [
          {
            "name": "state",
            "value": "State:Idle"
          }
        ]
      },
      {
        "instance": "sut.acquisition.sensor.port",
        "state": [
          {
            "name": "state",
            "value": "State:Idle"
          }
        ]
      },
      {
        "instance": "sut.acquisition.memory.port",
        "state": []
      }
    ],
    [
      {
        "instance": "control",
        "state": [
          {
            "name": "state",
            "value": "State:Idle"
          }
        ]
      },
      {
        "instance": "sut.driver",
        "state": [
          {
            "name": "state",
            "value": "State:Idle"
          }
        ]
      },
      {
        "instance": "sut.optics.optics",
        "state": [
          {
            "name": "state",
            "value": "State:Idle"
          },
          {
            "name": "ready",
            "value": "false"
          }
        ]
      },
      {
        "instance": "sut.optics.focus.focus",
        "state": [
          {
            "name": "idle",
            "value": "true"
          },
          {
            "name": "contrast_was",
            "value": "EContrast:Blurrier"
          }
        ]
      },
      {
        "instance": "sut.optics.focus.lens.port",
        "state": [
          {
            "name": "moving",
            "value": "false"
          }
        ]
      },
      {
        "instance": "sut.optics.focus.sensor.port",
        "state": []
      },
      {
        "instance": "sut.optics.shutter.port",
        "state": []
      },
      {
        "instance": "sut.acquisition.acquire",
        "state": [
          {
            "name": "state",
            "value": "State:Idle"
          }
        ]
      },
      {
        "instance": "sut.acquisition.sensor.port",
        "state": [
          {
            "name": "state",
            "value": "State:Idle"
          }
        ]
      },
      {
        "instance": "sut.acquisition.memory.port",
        "state": []
      }
    ]
  ]
}
