/*
 * Copyright (C) 2021 Rob Wieringa <rma.wieringa@gmail.com>
 *
 * This file is part of Dezyne-P5.
 * Dezyne-P5 offers Dezyne web views based on p5.js
 *
 * Dezyne-P5 is free software, it is distributed under the terms of
 * the GNU General Public Licence version 3 or later.
 * See <http://www.gnu.org/licenses/>.
 */

class IframeP5 {

  /*
   * interface to the outside world, through functions
   *   in.draw(data)
   *     show the data as a diagram
   *   in.dimensions(px, py, width, height)
   *     set the location (px, py) and size (with, height) of the
   *     diagram
   *   out.selected(location)
   *     process the selected node, using its location
   *
   *   out functions can be (re)defined by the outside world.
   */

  constructor(parent) {
    this.in = {
      draw: function (data) {
        this.sketch.data = data;
        if (this.sketch.set_up) {
          this.sketch.initDiagram(data);
          this.sketch.redraw();
        }
      }.bind(this),
      dimensions: function (px, py, width, height) {
        this.sketch.dimensions(px, py, width, height);
      }.bind(this),
    };
    this.out = {
    };

    // default dimensions: whole window
    this.px = 0;
    this.py = 0;
    this.width = window.innerWidth;
    this.height = window.innerHeight;

    // interface with sketch:
    this.sketch = new p5(this.iframeSketch);
    this.sketch.in = this.in;
    this.sketch.out = this.out;
    this.sketch.parent = parent;
    this.sketch.myPx = this.px;
    this.sketch.myPy = this.py;
    this.sketch.myWidth = this.width;
    this.sketch.myHeight = this.height;
  };

  iframeSketch(p) {
    // interface with surrounding P5 object:
    p.in = null;
    p.out = null;
    p.parent = null;
    p.myPx = null;
    p.myPy = null;
    p.myWidth = null;
    p.myHeight = null;

    p.dimensions = function(px, py, width, height) {
      p.myPx = px;
      p.myPy = py;
      p.myWidth = width;
      p.myHeight = height;
      if (p.set_up) {
        page.attribute('width', width);
        page.attribute('height', height);
      }
    };

    // initDiagram is accessed outside sketch
    // do not call before p5 setup!
    p.initDiagram = function(data) {
      p.data = data;
      page.attribute('src', data);
      p.noLoop();
    };

  let world = null;
  let seqdiag = null;

    p.setup = function() {
      let px = p.myPx;
      let py = p.myPy;
      let w = p.myWidth || p.windowWidth;
      let h = p.myHeight || p.windowHeight;

      page = createElement('iframe');
      if (parent) page.parent(parent);
      page.attribute('width', w);
      page.attribute('height', h);

      p.set_up = true;
      if (p.data) {
        p.initDiagram(p.data);
      }
      p.noLoop();
    };

    p.draw = function() {
    };

    function mouseInCanvas() {
      if (!p.myWidth && !p.myHeight) return true; // canvas spans whole window
      if (p.myWidth && !(0 <= p.mouseX && p.mouseX <= p.myWidth)) return false;
      if (p.myHeight && !(0 <= p.mouseY && p.mouseY <= p.myHeight)) return false;
      return true;
    }

    p.windowResized = function() {
      if (!p.myWidth && !p.myHeight)
        world.resizeCanvas(p.windowWidth, p.windowHeight);
    };

    p.mousePressed = function() {
      if (!mouseInCanvas()) return true;
    };

    p.mouseWheel = function(e) {
      if (!mouseInCanvas()) return true;
      if (!seqdiag) return;
      world.mouseWheel(e);
      p.redraw();
      return false;
    };
  };
};

