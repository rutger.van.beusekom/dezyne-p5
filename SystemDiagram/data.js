let hello_ast = {
  "<class>": "root",
  "elements": [
    {
      "<class>": "bool",
      "name": {
        "<class>": "scope_name",
        "ids": [
          "bool"
        ]
      }
    },
    {
      "<class>": "void",
      "name": {
        "<class>": "scope_name",
        "ids": [
          "void"
        ]
      }
    },
    {
      "<class>": "file-name",
      "name": "examples/hello.dzn"
    },
    {
      "<class>": "interface",
      "name": {
        "<class>": "scope_name",
        "ids": [
          "ihello"
        ]
      },
      "types": {
        "<class>": "types",
        "elements": []
      },
      "events": {
        "<class>": "events",
        "elements": [
          {
            "<class>": "event",
            "name": "hello",
            "signature": {
              "<class>": "signature",
              "type_name": {
                "<class>": "scope_name",
                "ids": [
                  "void"
                ]
              },
              "formals": {
                "<class>": "formals",
                "elements": []
              }
            },
            "direction": "in"
          }
        ]
      }
    },
    {
      "<class>": "component",
      "name": {
        "<class>": "scope_name",
        "ids": [
          "hello"
        ]
      },
      "ports": {
        "<class>": "ports",
        "elements": [
          {
            "<class>": "port",
            "name": "h",
            "type_name": {
              "<class>": "scope_name",
              "ids": [
                "ihello"
              ]
            },
            "direction": "provides",
            "formals": {
              "<class>": "formals",
              "elements": []
            }
          }
        ]
      }
    }
  ],
  "working-directory": "/home/janneke/src/verum/ide/wip-simulate"
};

let system_hello_ast = {
  "<class>": "root",
  "elements": [
    {
      "<class>": "bool",
      "name": {
        "<class>": "scope_name",
        "ids": [
          "bool"
        ]
      }
    },
    {
      "<class>": "void",
      "name": {
        "<class>": "scope_name",
        "ids": [
          "void"
        ]
      }
    },
    {
      "<class>": "file-name",
      "name": "examples/system-hello.dzn"
    },
    {
      "<class>": "interface",
      "name": {
        "<class>": "scope_name",
        "ids": [
          "ihello"
        ]
      },
      "types": {
        "<class>": "types",
        "elements": []
      },
      "events": {
        "<class>": "events",
        "elements": [
          {
            "<class>": "event",
            "name": "hello",
            "signature": {
              "<class>": "signature",
              "type_name": {
                "<class>": "scope_name",
                "ids": [
                  "void"
                ]
              },
              "formals": {
                "<class>": "formals",
                "elements": []
              }
            },
            "direction": "in"
          }
        ]
      }
    },
    {
      "<class>": "component",
      "name": {
        "<class>": "scope_name",
        "ids": [
          "hello"
        ]
      },
      "ports": {
        "<class>": "ports",
        "elements": [
          {
            "<class>": "port",
            "name": "hh",
            "type_name": {
              "<class>": "scope_name",
              "ids": [
                "ihello"
              ]
            },
            "direction": "provides",
            "formals": {
              "<class>": "formals",
              "elements": []
            }
          }
        ]
      }
    },
    {
      "<class>": "system",
      "name": {
        "<class>": "scope_name",
        "ids": [
          "system_hello"
        ]
      },
      "ports": {
        "<class>": "ports",
        "elements": [
          {
            "<class>": "port",
            "name": "h",
            "type_name": {
              "<class>": "scope_name",
              "ids": [
                "ihello"
              ]
            },
            "direction": "provides",
            "formals": {
              "<class>": "formals",
              "elements": []
            }
          }
        ]
      },
      "instances": {
        "<class>": "instances",
        "elements": [
          {
            "<class>": "instance",
            "name": "c",
            "type_name": {
              "<class>": "scope_name",
              "ids": [
                "hello"
              ]
            }
          }
        ]
      },
      "bindings": {
        "<class>": "bindings",
        "elements": [
          {
            "<class>": "binding",
            "left": {
              "<class>": "end-point",
              "port_name": "h"
            },
            "right": {
              "<class>": "end-point",
              "instance_name": "c",
              "port_name": "hh"
            }
          }
        ]
      }
    }
  ],
  "working-directory": "/home/janneke/src/verum/ide/wip-simulate"
};

let Camera_ast = {
  "<class>": "root",
  "location": {
    "<class>": "location",
    "file-name": "test/all/Camera/Camera.dzn",
    "line": 39,
    "column": 1,
    "end-line": 132,
    "end-column": 1,
    "offset": 1434,
    "length": 2067
  },
  "elements": [
    {
      "<class>": "bool",
      "name": {
        "<class>": "scope_name",
        "ids": [
          "bool"
        ]
      }
    },
    {
      "<class>": "void",
      "name": {
        "<class>": "scope_name",
        "ids": [
          "void"
        ]
      }
    },
    {
      "<class>": "file-name",
      "name": "test/all/Camera/Optics.dzn"
    },
    {
      "<class>": "interface",
      "location": {
        "<class>": "location",
        "file-name": "test/all/Camera/Optics.dzn",
        "line": 24,
        "column": 1,
        "end-line": 47,
        "end-column": 2,
        "offset": 827,
        "length": 396
      },
      "name": {
        "<class>": "scope_name",
        "ids": [
          "ILens"
        ]
      },
      "types": {
        "<class>": "types",
        "elements": []
      },
      "events": {
        "<class>": "events",
        "elements": [
          {
            "<class>": "event",
            "name": "forward",
            "signature": {
              "<class>": "signature",
              "type_name": {
                "<class>": "scope_name",
                "ids": [
                  "void"
                ]
              },
              "formals": {
                "<class>": "formals",
                "elements": []
              }
            },
            "direction": "in"
          },
          {
            "<class>": "event",
            "name": "backward",
            "signature": {
              "<class>": "signature",
              "type_name": {
                "<class>": "scope_name",
                "ids": [
                  "void"
                ]
              },
              "formals": {
                "<class>": "formals",
                "elements": []
              }
            },
            "direction": "in"
          },
          {
            "<class>": "event",
            "name": "stop",
            "signature": {
              "<class>": "signature",
              "type_name": {
                "<class>": "scope_name",
                "ids": [
                  "void"
                ]
              },
              "formals": {
                "<class>": "formals",
                "elements": []
              }
            },
            "direction": "in"
          },
          {
            "<class>": "event",
            "name": "stopped",
            "signature": {
              "<class>": "signature",
              "type_name": {
                "<class>": "scope_name",
                "ids": [
                  "void"
                ]
              },
              "formals": {
                "<class>": "formals",
                "elements": []
              }
            },
            "direction": "out"
          }
        ]
      }
    },
    {
      "<class>": "foreign",
      "location": {
        "<class>": "location",
        "file-name": "test/all/Camera/Optics.dzn",
        "line": 48,
        "column": 1,
        "end-line": 51,
        "end-column": 2,
        "offset": 1224,
        "length": 41
      },
      "name": {
        "<class>": "scope_name",
        "ids": [
          "Lens"
        ]
      },
      "ports": {
        "<class>": "ports",
        "elements": [
          {
            "<class>": "port",
            "location": {
              "<class>": "location",
              "file-name": "test/all/Camera/Optics.dzn",
              "line": 50,
              "column": 3,
              "end-line": 50,
              "end-column": 23,
              "offset": 1243,
              "length": 20
            },
            "name": "port",
            "type_name": {
              "<class>": "scope_name",
              "ids": [
                "ILens"
              ]
            },
            "direction": "provides",
            "formals": {
              "<class>": "formals",
              "elements": []
            }
          }
        ]
      }
    },
    {
      "<class>": "interface",
      "location": {
        "<class>": "location",
        "file-name": "test/all/Camera/Optics.dzn",
        "line": 52,
        "column": 1,
        "end-line": 66,
        "end-column": 2,
        "offset": 1266,
        "length": 273
      },
      "name": {
        "<class>": "scope_name",
        "ids": [
          "IFocus"
        ]
      },
      "types": {
        "<class>": "types",
        "elements": []
      },
      "events": {
        "<class>": "events",
        "elements": [
          {
            "<class>": "event",
            "name": "measure",
            "signature": {
              "<class>": "signature",
              "type_name": {
                "<class>": "scope_name",
                "ids": [
                  "void"
                ]
              },
              "formals": {
                "<class>": "formals",
                "elements": []
              }
            },
            "direction": "in"
          },
          {
            "<class>": "event",
            "name": "cancel",
            "signature": {
              "<class>": "signature",
              "type_name": {
                "<class>": "scope_name",
                "ids": [
                  "void"
                ]
              },
              "formals": {
                "<class>": "formals",
                "elements": []
              }
            },
            "direction": "in"
          },
          {
            "<class>": "event",
            "name": "maximum",
            "signature": {
              "<class>": "signature",
              "type_name": {
                "<class>": "scope_name",
                "ids": [
                  "void"
                ]
              },
              "formals": {
                "<class>": "formals",
                "elements": []
              }
            },
            "direction": "out"
          }
        ]
      }
    },
    {
      "<class>": "interface",
      "location": {
        "<class>": "location",
        "file-name": "test/all/Camera/Optics.dzn",
        "line": 67,
        "column": 1,
        "end-line": 77,
        "end-column": 2,
        "offset": 1540,
        "length": 193
      },
      "name": {
        "<class>": "scope_name",
        "ids": [
          "IContrast"
        ]
      },
      "types": {
        "<class>": "types",
        "elements": [
          {
            "<class>": "enum",
            "location": {
              "<class>": "location",
              "file-name": "test/all/Camera/Optics.dzn",
              "line": 69,
              "column": 3,
              "end-line": 69,
              "end-column": 38,
              "offset": 1564,
              "length": 35
            },
            "name": {
              "<class>": "scope_name",
              "ids": [
                "EContrast"
              ]
            },
            "fields": {
              "<class>": "fields",
              "elements": [
                "Sharper",
                "Blurrier"
              ]
            }
          }
        ]
      },
      "events": {
        "<class>": "events",
        "elements": [
          {
            "<class>": "event",
            "name": "measure",
            "signature": {
              "<class>": "signature",
              "type_name": {
                "<class>": "scope_name",
                "ids": [
                  "EContrast"
                ]
              },
              "formals": {
                "<class>": "formals",
                "elements": []
              }
            },
            "direction": "in"
          }
        ]
      }
    },
    {
      "<class>": "foreign",
      "location": {
        "<class>": "location",
        "file-name": "test/all/Camera/Optics.dzn",
        "line": 78,
        "column": 1,
        "end-line": 81,
        "end-column": 2,
        "offset": 1734,
        "length": 49
      },
      "name": {
        "<class>": "scope_name",
        "ids": [
          "Contrast"
        ]
      },
      "ports": {
        "<class>": "ports",
        "elements": [
          {
            "<class>": "port",
            "location": {
              "<class>": "location",
              "file-name": "test/all/Camera/Optics.dzn",
              "line": 80,
              "column": 3,
              "end-line": 80,
              "end-column": 27,
              "offset": 1757,
              "length": 24
            },
            "name": "port",
            "type_name": {
              "<class>": "scope_name",
              "ids": [
                "IContrast"
              ]
            },
            "direction": "provides",
            "formals": {
              "<class>": "formals",
              "elements": []
            }
          }
        ]
      }
    },
    {
      "<class>": "system",
      "location": {
        "<class>": "location",
        "file-name": "test/all/Camera/Optics.dzn",
        "line": 83,
        "column": 1,
        "end-line": 95,
        "end-column": 2,
        "offset": 1785,
        "length": 208
      },
      "name": {
        "<class>": "scope_name",
        "ids": [
          "Focus"
        ]
      },
      "ports": {
        "<class>": "ports",
        "elements": [
          {
            "<class>": "port",
            "location": {
              "<class>": "location",
              "file-name": "test/all/Camera/Optics.dzn",
              "line": 85,
              "column": 3,
              "end-line": 85,
              "end-column": 24,
              "offset": 1805,
              "length": 21
            },
            "name": "port",
            "type_name": {
              "<class>": "scope_name",
              "ids": [
                "IFocus"
              ]
            },
            "direction": "provides",
            "formals": {
              "<class>": "formals",
              "elements": []
            }
          }
        ]
      },
      "instances": {
        "<class>": "instances",
        "elements": [
          {
            "<class>": "instance",
            "location": {
              "<class>": "location",
              "file-name": "test/all/Camera/Optics.dzn",
              "line": 87,
              "column": 5,
              "end-line": 87,
              "end-column": 24,
              "offset": 1842,
              "length": 19
            },
            "name": "focus",
            "type_name": {
              "<class>": "scope_name",
              "ids": [
                "FocusControl"
              ]
            }
          },
          {
            "<class>": "instance",
            "location": {
              "<class>": "location",
              "file-name": "test/all/Camera/Optics.dzn",
              "line": 88,
              "column": 5,
              "end-line": 88,
              "end-column": 21,
              "offset": 1866,
              "length": 16
            },
            "name": "sensor",
            "type_name": {
              "<class>": "scope_name",
              "ids": [
                "Contrast"
              ]
            }
          },
          {
            "<class>": "instance",
            "location": {
              "<class>": "location",
              "file-name": "test/all/Camera/Optics.dzn",
              "line": 89,
              "column": 5,
              "end-line": 89,
              "end-column": 15,
              "offset": 1887,
              "length": 10
            },
            "name": "lens",
            "type_name": {
              "<class>": "scope_name",
              "ids": [
                "Lens"
              ]
            }
          }
        ]
      },
      "bindings": {
        "<class>": "bindings",
        "elements": [
          {
            "<class>": "binding",
            "location": {
              "<class>": "location",
              "file-name": "test/all/Camera/Optics.dzn",
              "line": 91,
              "column": 5,
              "end-line": 91,
              "end-column": 25,
              "offset": 1903,
              "length": 20
            },
            "left": {
              "<class>": "end-point",
              "port_name": "port"
            },
            "right": {
              "<class>": "end-point",
              "instance_name": "focus",
              "port_name": "port"
            }
          },
          {
            "<class>": "binding",
            "location": {
              "<class>": "location",
              "file-name": "test/all/Camera/Optics.dzn",
              "line": 92,
              "column": 5,
              "end-line": 92,
              "end-column": 30,
              "offset": 1928,
              "length": 25
            },
            "left": {
              "<class>": "end-point",
              "instance_name": "focus",
              "port_name": "lens"
            },
            "right": {
              "<class>": "end-point",
              "instance_name": "lens",
              "port_name": "port"
            }
          },
          {
            "<class>": "binding",
            "location": {
              "<class>": "location",
              "file-name": "test/all/Camera/Optics.dzn",
              "line": 93,
              "column": 5,
              "end-line": 93,
              "end-column": 34,
              "offset": 1958,
              "length": 29
            },
            "left": {
              "<class>": "end-point",
              "instance_name": "focus",
              "port_name": "sensor"
            },
            "right": {
              "<class>": "end-point",
              "instance_name": "sensor",
              "port_name": "port"
            }
          }
        ]
      }
    },
    {
      "<class>": "component",
      "location": {
        "<class>": "location",
        "file-name": "test/all/Camera/Optics.dzn",
        "line": 97,
        "column": 1,
        "end-line": 118,
        "end-column": 2,
        "offset": 1995,
        "length": 680
      },
      "name": {
        "<class>": "scope_name",
        "ids": [
          "FocusControl"
        ]
      },
      "ports": {
        "<class>": "ports",
        "elements": [
          {
            "<class>": "port",
            "location": {
              "<class>": "location",
              "file-name": "test/all/Camera/Optics.dzn",
              "line": 99,
              "column": 3,
              "end-line": 99,
              "end-column": 24,
              "offset": 2022,
              "length": 21
            },
            "name": "port",
            "type_name": {
              "<class>": "scope_name",
              "ids": [
                "IFocus"
              ]
            },
            "direction": "provides",
            "formals": {
              "<class>": "formals",
              "elements": []
            }
          },
          {
            "<class>": "port",
            "location": {
              "<class>": "location",
              "file-name": "test/all/Camera/Optics.dzn",
              "line": 101,
              "column": 3,
              "end-line": 101,
              "end-column": 23,
              "offset": 2047,
              "length": 20
            },
            "name": "lens",
            "type_name": {
              "<class>": "scope_name",
              "ids": [
                "ILens"
              ]
            },
            "direction": "requires",
            "formals": {
              "<class>": "formals",
              "elements": []
            }
          },
          {
            "<class>": "port",
            "location": {
              "<class>": "location",
              "file-name": "test/all/Camera/Optics.dzn",
              "line": 102,
              "column": 3,
              "end-line": 102,
              "end-column": 29,
              "offset": 2070,
              "length": 26
            },
            "name": "sensor",
            "type_name": {
              "<class>": "scope_name",
              "ids": [
                "IContrast"
              ]
            },
            "direction": "requires",
            "formals": {
              "<class>": "formals",
              "elements": []
            }
          }
        ]
      }
    },
    {
      "<class>": "interface",
      "location": {
        "<class>": "location",
        "file-name": "test/all/Camera/Optics.dzn",
        "line": 120,
        "column": 1,
        "end-line": 127,
        "end-column": 2,
        "offset": 2677,
        "length": 80
      },
      "name": {
        "<class>": "scope_name",
        "ids": [
          "IShutter"
        ]
      },
      "types": {
        "<class>": "types",
        "elements": []
      },
      "events": {
        "<class>": "events",
        "elements": [
          {
            "<class>": "event",
            "name": "expose",
            "signature": {
              "<class>": "signature",
              "type_name": {
                "<class>": "scope_name",
                "ids": [
                  "void"
                ]
              },
              "formals": {
                "<class>": "formals",
                "elements": []
              }
            },
            "direction": "in"
          }
        ]
      }
    },
    {
      "<class>": "foreign",
      "location": {
        "<class>": "location",
        "file-name": "test/all/Camera/Optics.dzn",
        "line": 128,
        "column": 1,
        "end-line": 131,
        "end-column": 2,
        "offset": 2758,
        "length": 47
      },
      "name": {
        "<class>": "scope_name",
        "ids": [
          "Shutter"
        ]
      },
      "ports": {
        "<class>": "ports",
        "elements": [
          {
            "<class>": "port",
            "location": {
              "<class>": "location",
              "file-name": "test/all/Camera/Optics.dzn",
              "line": 130,
              "column": 3,
              "end-line": 130,
              "end-column": 26,
              "offset": 2780,
              "length": 23
            },
            "name": "port",
            "type_name": {
              "<class>": "scope_name",
              "ids": [
                "IShutter"
              ]
            },
            "direction": "provides",
            "formals": {
              "<class>": "formals",
              "elements": []
            }
          }
        ]
      }
    },
    {
      "<class>": "interface",
      "location": {
        "<class>": "location",
        "file-name": "test/all/Camera/Optics.dzn",
        "line": 133,
        "column": 1,
        "end-line": 160,
        "end-column": 2,
        "offset": 2807,
        "length": 502
      },
      "name": {
        "<class>": "scope_name",
        "ids": [
          "IOptics"
        ]
      },
      "types": {
        "<class>": "types",
        "elements": []
      },
      "events": {
        "<class>": "events",
        "elements": [
          {
            "<class>": "event",
            "name": "prepare",
            "signature": {
              "<class>": "signature",
              "type_name": {
                "<class>": "scope_name",
                "ids": [
                  "void"
                ]
              },
              "formals": {
                "<class>": "formals",
                "elements": []
              }
            },
            "direction": "in"
          },
          {
            "<class>": "event",
            "name": "capture",
            "signature": {
              "<class>": "signature",
              "type_name": {
                "<class>": "scope_name",
                "ids": [
                  "void"
                ]
              },
              "formals": {
                "<class>": "formals",
                "elements": []
              }
            },
            "direction": "in"
          },
          {
            "<class>": "event",
            "name": "cancel",
            "signature": {
              "<class>": "signature",
              "type_name": {
                "<class>": "scope_name",
                "ids": [
                  "void"
                ]
              },
              "formals": {
                "<class>": "formals",
                "elements": []
              }
            },
            "direction": "in"
          },
          {
            "<class>": "event",
            "name": "ready",
            "signature": {
              "<class>": "signature",
              "type_name": {
                "<class>": "scope_name",
                "ids": [
                  "void"
                ]
              },
              "formals": {
                "<class>": "formals",
                "elements": []
              }
            },
            "direction": "out"
          }
        ]
      }
    },
    {
      "<class>": "component",
      "location": {
        "<class>": "location",
        "file-name": "test/all/Camera/Optics.dzn",
        "line": 161,
        "column": 1,
        "end-line": 189,
        "end-column": 2,
        "offset": 3310,
        "length": 651
      },
      "name": {
        "<class>": "scope_name",
        "ids": [
          "OpticsControl"
        ]
      },
      "ports": {
        "<class>": "ports",
        "elements": [
          {
            "<class>": "port",
            "location": {
              "<class>": "location",
              "file-name": "test/all/Camera/Optics.dzn",
              "line": 163,
              "column": 3,
              "end-line": 163,
              "end-column": 25,
              "offset": 3338,
              "length": 22
            },
            "name": "port",
            "type_name": {
              "<class>": "scope_name",
              "ids": [
                "IOptics"
              ]
            },
            "direction": "provides",
            "formals": {
              "<class>": "formals",
              "elements": []
            }
          },
          {
            "<class>": "port",
            "location": {
              "<class>": "location",
              "file-name": "test/all/Camera/Optics.dzn",
              "line": 165,
              "column": 3,
              "end-line": 165,
              "end-column": 25,
              "offset": 3364,
              "length": 22
            },
            "name": "focus",
            "type_name": {
              "<class>": "scope_name",
              "ids": [
                "IFocus"
              ]
            },
            "direction": "requires",
            "formals": {
              "<class>": "formals",
              "elements": []
            }
          },
          {
            "<class>": "port",
            "location": {
              "<class>": "location",
              "file-name": "test/all/Camera/Optics.dzn",
              "line": 166,
              "column": 3,
              "end-line": 166,
              "end-column": 29,
              "offset": 3389,
              "length": 26
            },
            "name": "shutter",
            "type_name": {
              "<class>": "scope_name",
              "ids": [
                "IShutter"
              ]
            },
            "direction": "requires",
            "formals": {
              "<class>": "formals",
              "elements": []
            }
          }
        ]
      }
    },
    {
      "<class>": "system",
      "location": {
        "<class>": "location",
        "file-name": "test/all/Camera/Optics.dzn",
        "line": 190,
        "column": 1,
        "end-line": 207,
        "end-column": 2,
        "offset": 3962,
        "length": 227
      },
      "name": {
        "<class>": "scope_name",
        "ids": [
          "Optics"
        ]
      },
      "ports": {
        "<class>": "ports",
        "elements": [
          {
            "<class>": "port",
            "location": {
              "<class>": "location",
              "file-name": "test/all/Camera/Optics.dzn",
              "line": 192,
              "column": 3,
              "end-line": 192,
              "end-column": 25,
              "offset": 3983,
              "length": 22
            },
            "name": "port",
            "type_name": {
              "<class>": "scope_name",
              "ids": [
                "IOptics"
              ]
            },
            "direction": "provides",
            "formals": {
              "<class>": "formals",
              "elements": []
            }
          }
        ]
      },
      "instances": {
        "<class>": "instances",
        "elements": [
          {
            "<class>": "instance",
            "location": {
              "<class>": "location",
              "file-name": "test/all/Camera/Optics.dzn",
              "line": 196,
              "column": 5,
              "end-line": 196,
              "end-column": 26,
              "offset": 4024,
              "length": 21
            },
            "name": "optics",
            "type_name": {
              "<class>": "scope_name",
              "ids": [
                "OpticsControl"
              ]
            }
          },
          {
            "<class>": "instance",
            "location": {
              "<class>": "location",
              "file-name": "test/all/Camera/Optics.dzn",
              "line": 198,
              "column": 5,
              "end-line": 198,
              "end-column": 21,
              "offset": 4051,
              "length": 16
            },
            "name": "shutter",
            "type_name": {
              "<class>": "scope_name",
              "ids": [
                "Shutter"
              ]
            }
          },
          {
            "<class>": "instance",
            "location": {
              "<class>": "location",
              "file-name": "test/all/Camera/Optics.dzn",
              "line": 199,
              "column": 5,
              "end-line": 199,
              "end-column": 17,
              "offset": 4072,
              "length": 12
            },
            "name": "focus",
            "type_name": {
              "<class>": "scope_name",
              "ids": [
                "Focus"
              ]
            }
          }
        ]
      },
      "bindings": {
        "<class>": "bindings",
        "elements": [
          {
            "<class>": "binding",
            "location": {
              "<class>": "location",
              "file-name": "test/all/Camera/Optics.dzn",
              "line": 201,
              "column": 5,
              "end-line": 201,
              "end-column": 26,
              "offset": 4090,
              "length": 21
            },
            "left": {
              "<class>": "end-point",
              "port_name": "port"
            },
            "right": {
              "<class>": "end-point",
              "instance_name": "optics",
              "port_name": "port"
            }
          },
          {
            "<class>": "binding",
            "location": {
              "<class>": "location",
              "file-name": "test/all/Camera/Optics.dzn",
              "line": 203,
              "column": 5,
              "end-line": 203,
              "end-column": 37,
              "offset": 4117,
              "length": 32
            },
            "left": {
              "<class>": "end-point",
              "instance_name": "optics",
              "port_name": "shutter"
            },
            "right": {
              "<class>": "end-point",
              "instance_name": "shutter",
              "port_name": "port"
            }
          },
          {
            "<class>": "binding",
            "location": {
              "<class>": "location",
              "file-name": "test/all/Camera/Optics.dzn",
              "line": 205,
              "column": 5,
              "end-line": 205,
              "end-column": 33,
              "offset": 4155,
              "length": 28
            },
            "left": {
              "<class>": "end-point",
              "instance_name": "optics",
              "port_name": "focus"
            },
            "right": {
              "<class>": "end-point",
              "instance_name": "focus",
              "port_name": "port"
            }
          }
        ]
      }
    },
    {
      "<class>": "file-name",
      "name": "test/all/Camera/Acquisition.dzn"
    },
    {
      "<class>": "interface",
      "location": {
        "<class>": "location",
        "file-name": "test/all/Camera/Acquisition.dzn",
        "line": 24,
        "column": 1,
        "end-line": 54,
        "end-column": 2,
        "offset": 833,
        "length": 542
      },
      "name": {
        "<class>": "scope_name",
        "ids": [
          "ISensor"
        ]
      },
      "types": {
        "<class>": "types",
        "elements": []
      },
      "events": {
        "<class>": "events",
        "elements": [
          {
            "<class>": "event",
            "name": "prepare",
            "signature": {
              "<class>": "signature",
              "type_name": {
                "<class>": "scope_name",
                "ids": [
                  "void"
                ]
              },
              "formals": {
                "<class>": "formals",
                "elements": []
              }
            },
            "direction": "in"
          },
          {
            "<class>": "event",
            "name": "acquire",
            "signature": {
              "<class>": "signature",
              "type_name": {
                "<class>": "scope_name",
                "ids": [
                  "void"
                ]
              },
              "formals": {
                "<class>": "formals",
                "elements": []
              }
            },
            "direction": "in"
          },
          {
            "<class>": "event",
            "name": "cancel",
            "signature": {
              "<class>": "signature",
              "type_name": {
                "<class>": "scope_name",
                "ids": [
                  "void"
                ]
              },
              "formals": {
                "<class>": "formals",
                "elements": []
              }
            },
            "direction": "in"
          },
          {
            "<class>": "event",
            "name": "image",
            "signature": {
              "<class>": "signature",
              "type_name": {
                "<class>": "scope_name",
                "ids": [
                  "void"
                ]
              },
              "formals": {
                "<class>": "formals",
                "elements": []
              }
            },
            "direction": "out"
          }
        ]
      }
    },
    {
      "<class>": "foreign",
      "location": {
        "<class>": "location",
        "file-name": "test/all/Camera/Acquisition.dzn",
        "line": 56,
        "column": 1,
        "end-line": 59,
        "end-column": 2,
        "offset": 1377,
        "length": 43
      },
      "name": {
        "<class>": "scope_name",
        "ids": [
          "CMOS"
        ]
      },
      "ports": {
        "<class>": "ports",
        "elements": [
          {
            "<class>": "port",
            "location": {
              "<class>": "location",
              "file-name": "test/all/Camera/Acquisition.dzn",
              "line": 58,
              "column": 3,
              "end-line": 58,
              "end-column": 25,
              "offset": 1396,
              "length": 22
            },
            "name": "port",
            "type_name": {
              "<class>": "scope_name",
              "ids": [
                "ISensor"
              ]
            },
            "direction": "provides",
            "formals": {
              "<class>": "formals",
              "elements": []
            }
          }
        ]
      }
    },
    {
      "<class>": "interface",
      "location": {
        "<class>": "location",
        "file-name": "test/all/Camera/Acquisition.dzn",
        "line": 61,
        "column": 1,
        "end-line": 68,
        "end-column": 2,
        "offset": 1422,
        "length": 76
      },
      "name": {
        "<class>": "scope_name",
        "ids": [
          "IMemory"
        ]
      },
      "types": {
        "<class>": "types",
        "elements": []
      },
      "events": {
        "<class>": "events",
        "elements": [
          {
            "<class>": "event",
            "name": "store",
            "signature": {
              "<class>": "signature",
              "type_name": {
                "<class>": "scope_name",
                "ids": [
                  "void"
                ]
              },
              "formals": {
                "<class>": "formals",
                "elements": []
              }
            },
            "direction": "in"
          }
        ]
      }
    },
    {
      "<class>": "foreign",
      "location": {
        "<class>": "location",
        "file-name": "test/all/Camera/Acquisition.dzn",
        "line": 70,
        "column": 1,
        "end-line": 73,
        "end-column": 2,
        "offset": 1500,
        "length": 45
      },
      "name": {
        "<class>": "scope_name",
        "ids": [
          "Memory"
        ]
      },
      "ports": {
        "<class>": "ports",
        "elements": [
          {
            "<class>": "port",
            "location": {
              "<class>": "location",
              "file-name": "test/all/Camera/Acquisition.dzn",
              "line": 72,
              "column": 3,
              "end-line": 72,
              "end-column": 25,
              "offset": 1521,
              "length": 22
            },
            "name": "port",
            "type_name": {
              "<class>": "scope_name",
              "ids": [
                "IMemory"
              ]
            },
            "direction": "provides",
            "formals": {
              "<class>": "formals",
              "elements": []
            }
          }
        ]
      }
    },
    {
      "<class>": "interface",
      "location": {
        "<class>": "location",
        "file-name": "test/all/Camera/Acquisition.dzn",
        "line": 75,
        "column": 1,
        "end-line": 108,
        "end-column": 2,
        "offset": 1547,
        "length": 555
      },
      "name": {
        "<class>": "scope_name",
        "ids": [
          "IAcquisition"
        ]
      },
      "types": {
        "<class>": "types",
        "elements": []
      },
      "events": {
        "<class>": "events",
        "elements": [
          {
            "<class>": "event",
            "name": "prepare",
            "signature": {
              "<class>": "signature",
              "type_name": {
                "<class>": "scope_name",
                "ids": [
                  "void"
                ]
              },
              "formals": {
                "<class>": "formals",
                "elements": []
              }
            },
            "direction": "in"
          },
          {
            "<class>": "event",
            "name": "cancel",
            "signature": {
              "<class>": "signature",
              "type_name": {
                "<class>": "scope_name",
                "ids": [
                  "void"
                ]
              },
              "formals": {
                "<class>": "formals",
                "elements": []
              }
            },
            "direction": "in"
          },
          {
            "<class>": "event",
            "name": "acquire",
            "signature": {
              "<class>": "signature",
              "type_name": {
                "<class>": "scope_name",
                "ids": [
                  "void"
                ]
              },
              "formals": {
                "<class>": "formals",
                "elements": []
              }
            },
            "direction": "in"
          },
          {
            "<class>": "event",
            "name": "image",
            "signature": {
              "<class>": "signature",
              "type_name": {
                "<class>": "scope_name",
                "ids": [
                  "void"
                ]
              },
              "formals": {
                "<class>": "formals",
                "elements": []
              }
            },
            "direction": "out"
          },
          {
            "<class>": "event",
            "name": "focus",
            "signature": {
              "<class>": "signature",
              "type_name": {
                "<class>": "scope_name",
                "ids": [
                  "void"
                ]
              },
              "formals": {
                "<class>": "formals",
                "elements": []
              }
            },
            "direction": "out"
          }
        ]
      }
    },
    {
      "<class>": "component",
      "location": {
        "<class>": "location",
        "file-name": "test/all/Camera/Acquisition.dzn",
        "line": 110,
        "column": 1,
        "end-line": 140,
        "end-column": 2,
        "offset": 2104,
        "length": 595
      },
      "name": {
        "<class>": "scope_name",
        "ids": [
          "Acquire"
        ]
      },
      "ports": {
        "<class>": "ports",
        "elements": [
          {
            "<class>": "port",
            "location": {
              "<class>": "location",
              "file-name": "test/all/Camera/Acquisition.dzn",
              "line": 112,
              "column": 3,
              "end-line": 112,
              "end-column": 30,
              "offset": 2126,
              "length": 27
            },
            "name": "port",
            "type_name": {
              "<class>": "scope_name",
              "ids": [
                "IAcquisition"
              ]
            },
            "direction": "provides",
            "formals": {
              "<class>": "formals",
              "elements": []
            }
          },
          {
            "<class>": "port",
            "location": {
              "<class>": "location",
              "file-name": "test/all/Camera/Acquisition.dzn",
              "line": 113,
              "column": 3,
              "end-line": 113,
              "end-column": 27,
              "offset": 2156,
              "length": 24
            },
            "name": "memory",
            "type_name": {
              "<class>": "scope_name",
              "ids": [
                "IMemory"
              ]
            },
            "direction": "requires",
            "formals": {
              "<class>": "formals",
              "elements": []
            }
          },
          {
            "<class>": "port",
            "location": {
              "<class>": "location",
              "file-name": "test/all/Camera/Acquisition.dzn",
              "line": 114,
              "column": 3,
              "end-line": 114,
              "end-column": 27,
              "offset": 2183,
              "length": 24
            },
            "name": "sensor",
            "type_name": {
              "<class>": "scope_name",
              "ids": [
                "ISensor"
              ]
            },
            "direction": "requires",
            "formals": {
              "<class>": "formals",
              "elements": []
            }
          }
        ]
      }
    },
    {
      "<class>": "system",
      "location": {
        "<class>": "location",
        "file-name": "test/all/Camera/Acquisition.dzn",
        "line": 142,
        "column": 1,
        "end-line": 157,
        "end-column": 2,
        "offset": 2701,
        "length": 231
      },
      "name": {
        "<class>": "scope_name",
        "ids": [
          "Acquisition"
        ]
      },
      "ports": {
        "<class>": "ports",
        "elements": [
          {
            "<class>": "port",
            "location": {
              "<class>": "location",
              "file-name": "test/all/Camera/Acquisition.dzn",
              "line": 144,
              "column": 3,
              "end-line": 144,
              "end-column": 30,
              "offset": 2727,
              "length": 27
            },
            "name": "port",
            "type_name": {
              "<class>": "scope_name",
              "ids": [
                "IAcquisition"
              ]
            },
            "direction": "provides",
            "formals": {
              "<class>": "formals",
              "elements": []
            }
          }
        ]
      },
      "instances": {
        "<class>": "instances",
        "elements": [
          {
            "<class>": "instance",
            "location": {
              "<class>": "location",
              "file-name": "test/all/Camera/Acquisition.dzn",
              "line": 148,
              "column": 5,
              "end-line": 148,
              "end-column": 21,
              "offset": 2773,
              "length": 16
            },
            "name": "acquire",
            "type_name": {
              "<class>": "scope_name",
              "ids": [
                "Acquire"
              ]
            }
          },
          {
            "<class>": "instance",
            "location": {
              "<class>": "location",
              "file-name": "test/all/Camera/Acquisition.dzn",
              "line": 149,
              "column": 5,
              "end-line": 149,
              "end-column": 19,
              "offset": 2794,
              "length": 14
            },
            "name": "memory",
            "type_name": {
              "<class>": "scope_name",
              "ids": [
                "Memory"
              ]
            }
          },
          {
            "<class>": "instance",
            "location": {
              "<class>": "location",
              "file-name": "test/all/Camera/Acquisition.dzn",
              "line": 150,
              "column": 5,
              "end-line": 150,
              "end-column": 17,
              "offset": 2813,
              "length": 12
            },
            "name": "sensor",
            "type_name": {
              "<class>": "scope_name",
              "ids": [
                "CMOS"
              ]
            }
          }
        ]
      },
      "bindings": {
        "<class>": "bindings",
        "elements": [
          {
            "<class>": "binding",
            "location": {
              "<class>": "location",
              "file-name": "test/all/Camera/Acquisition.dzn",
              "line": 152,
              "column": 5,
              "end-line": 152,
              "end-column": 27,
              "offset": 2831,
              "length": 22
            },
            "left": {
              "<class>": "end-point",
              "port_name": "port"
            },
            "right": {
              "<class>": "end-point",
              "instance_name": "acquire",
              "port_name": "port"
            }
          },
          {
            "<class>": "binding",
            "location": {
              "<class>": "location",
              "file-name": "test/all/Camera/Acquisition.dzn",
              "line": 154,
              "column": 5,
              "end-line": 154,
              "end-column": 36,
              "offset": 2859,
              "length": 31
            },
            "left": {
              "<class>": "end-point",
              "instance_name": "acquire",
              "port_name": "memory"
            },
            "right": {
              "<class>": "end-point",
              "instance_name": "memory",
              "port_name": "port"
            }
          },
          {
            "<class>": "binding",
            "location": {
              "<class>": "location",
              "file-name": "test/all/Camera/Acquisition.dzn",
              "line": 155,
              "column": 5,
              "end-line": 155,
              "end-column": 36,
              "offset": 2895,
              "length": 31
            },
            "left": {
              "<class>": "end-point",
              "instance_name": "acquire",
              "port_name": "sensor"
            },
            "right": {
              "<class>": "end-point",
              "instance_name": "sensor",
              "port_name": "port"
            }
          }
        ]
      }
    },
    {
      "<class>": "file-name",
      "name": "test/all/Camera/Camera.dzn"
    },
    {
      "<class>": "interface",
      "location": {
        "<class>": "location",
        "file-name": "test/all/Camera/Camera.dzn",
        "line": 42,
        "column": 1,
        "end-line": 79,
        "end-column": 2,
        "offset": 1478,
        "length": 791
      },
      "name": {
        "<class>": "scope_name",
        "ids": [
          "IControl"
        ]
      },
      "types": {
        "<class>": "types",
        "elements": []
      },
      "events": {
        "<class>": "events",
        "elements": [
          {
            "<class>": "event",
            "name": "setup",
            "signature": {
              "<class>": "signature",
              "type_name": {
                "<class>": "scope_name",
                "ids": [
                  "void"
                ]
              },
              "formals": {
                "<class>": "formals",
                "elements": []
              }
            },
            "direction": "in"
          },
          {
            "<class>": "event",
            "name": "shoot",
            "signature": {
              "<class>": "signature",
              "type_name": {
                "<class>": "scope_name",
                "ids": [
                  "void"
                ]
              },
              "formals": {
                "<class>": "formals",
                "elements": []
              }
            },
            "direction": "in"
          },
          {
            "<class>": "event",
            "name": "release",
            "signature": {
              "<class>": "signature",
              "type_name": {
                "<class>": "scope_name",
                "ids": [
                  "void"
                ]
              },
              "formals": {
                "<class>": "formals",
                "elements": []
              }
            },
            "direction": "in"
          },
          {
            "<class>": "event",
            "name": "focus_lock",
            "signature": {
              "<class>": "signature",
              "type_name": {
                "<class>": "scope_name",
                "ids": [
                  "void"
                ]
              },
              "formals": {
                "<class>": "formals",
                "elements": []
              }
            },
            "direction": "out"
          },
          {
            "<class>": "event",
            "name": "image",
            "signature": {
              "<class>": "signature",
              "type_name": {
                "<class>": "scope_name",
                "ids": [
                  "void"
                ]
              },
              "formals": {
                "<class>": "formals",
                "elements": []
              }
            },
            "direction": "out"
          }
        ]
      }
    },
    {
      "<class>": "component",
      "location": {
        "<class>": "location",
        "file-name": "test/all/Camera/Camera.dzn",
        "line": 81,
        "column": 1,
        "end-line": 115,
        "end-column": 2,
        "offset": 2271,
        "length": 980
      },
      "name": {
        "<class>": "scope_name",
        "ids": [
          "Driver"
        ]
      },
      "ports": {
        "<class>": "ports",
        "elements": [
          {
            "<class>": "port",
            "location": {
              "<class>": "location",
              "file-name": "test/all/Camera/Camera.dzn",
              "line": 83,
              "column": 3,
              "end-line": 83,
              "end-column": 29,
              "offset": 2292,
              "length": 26
            },
            "name": "control",
            "type_name": {
              "<class>": "scope_name",
              "ids": [
                "IControl"
              ]
            },
            "direction": "provides",
            "formals": {
              "<class>": "formals",
              "elements": []
            }
          },
          {
            "<class>": "port",
            "location": {
              "<class>": "location",
              "file-name": "test/all/Camera/Camera.dzn",
              "line": 84,
              "column": 3,
              "end-line": 84,
              "end-column": 37,
              "offset": 2321,
              "length": 34
            },
            "name": "acquisition",
            "type_name": {
              "<class>": "scope_name",
              "ids": [
                "IAcquisition"
              ]
            },
            "direction": "requires",
            "formals": {
              "<class>": "formals",
              "elements": []
            }
          },
          {
            "<class>": "port",
            "location": {
              "<class>": "location",
              "file-name": "test/all/Camera/Camera.dzn",
              "line": 85,
              "column": 3,
              "end-line": 85,
              "end-column": 27,
              "offset": 2358,
              "length": 24
            },
            "name": "optics",
            "type_name": {
              "<class>": "scope_name",
              "ids": [
                "IOptics"
              ]
            },
            "direction": "requires",
            "formals": {
              "<class>": "formals",
              "elements": []
            }
          }
        ]
      }
    },
    {
      "<class>": "system",
      "location": {
        "<class>": "location",
        "file-name": "test/all/Camera/Camera.dzn",
        "line": 117,
        "column": 1,
        "end-line": 131,
        "end-column": 2,
        "offset": 3253,
        "length": 247
      },
      "name": {
        "<class>": "scope_name",
        "ids": [
          "Camera"
        ]
      },
      "ports": {
        "<class>": "ports",
        "elements": [
          {
            "<class>": "port",
            "location": {
              "<class>": "location",
              "file-name": "test/all/Camera/Camera.dzn",
              "line": 119,
              "column": 3,
              "end-line": 119,
              "end-column": 29,
              "offset": 3274,
              "length": 26
            },
            "name": "control",
            "type_name": {
              "<class>": "scope_name",
              "ids": [
                "IControl"
              ]
            },
            "direction": "provides",
            "formals": {
              "<class>": "formals",
              "elements": []
            }
          }
        ]
      },
      "instances": {
        "<class>": "instances",
        "elements": [
          {
            "<class>": "instance",
            "location": {
              "<class>": "location",
              "file-name": "test/all/Camera/Camera.dzn",
              "line": 123,
              "column": 5,
              "end-line": 123,
              "end-column": 19,
              "offset": 3319,
              "length": 14
            },
            "name": "driver",
            "type_name": {
              "<class>": "scope_name",
              "ids": [
                "Driver"
              ]
            }
          },
          {
            "<class>": "instance",
            "location": {
              "<class>": "location",
              "file-name": "test/all/Camera/Camera.dzn",
              "line": 124,
              "column": 5,
              "end-line": 124,
              "end-column": 29,
              "offset": 3338,
              "length": 24
            },
            "name": "acquisition",
            "type_name": {
              "<class>": "scope_name",
              "ids": [
                "Acquisition"
              ]
            }
          },
          {
            "<class>": "instance",
            "location": {
              "<class>": "location",
              "file-name": "test/all/Camera/Camera.dzn",
              "line": 125,
              "column": 5,
              "end-line": 125,
              "end-column": 19,
              "offset": 3367,
              "length": 14
            },
            "name": "optics",
            "type_name": {
              "<class>": "scope_name",
              "ids": [
                "Optics"
              ]
            }
          }
        ]
      },
      "bindings": {
        "<class>": "bindings",
        "elements": [
          {
            "<class>": "binding",
            "location": {
              "<class>": "location",
              "file-name": "test/all/Camera/Camera.dzn",
              "line": 127,
              "column": 5,
              "end-line": 127,
              "end-column": 32,
              "offset": 3387,
              "length": 27
            },
            "left": {
              "<class>": "end-point",
              "port_name": "control"
            },
            "right": {
              "<class>": "end-point",
              "instance_name": "driver",
              "port_name": "control"
            }
          },
          {
            "<class>": "binding",
            "location": {
              "<class>": "location",
              "file-name": "test/all/Camera/Camera.dzn",
              "line": 128,
              "column": 5,
              "end-line": 128,
              "end-column": 45,
              "offset": 3419,
              "length": 40
            },
            "left": {
              "<class>": "end-point",
              "instance_name": "driver",
              "port_name": "acquisition"
            },
            "right": {
              "<class>": "end-point",
              "instance_name": "acquisition",
              "port_name": "port"
            }
          },
          {
            "<class>": "binding",
            "location": {
              "<class>": "location",
              "file-name": "test/all/Camera/Camera.dzn",
              "line": 129,
              "column": 5,
              "end-line": 129,
              "end-column": 35,
              "offset": 3464,
              "length": 30
            },
            "left": {
              "<class>": "end-point",
              "instance_name": "driver",
              "port_name": "optics"
            },
            "right": {
              "<class>": "end-point",
              "instance_name": "optics",
              "port_name": "port"
            }
          }
        ]
      }
    }
  ],
  "working-directory": "/home/robw/GEN2/dzn"
}
;

let erroneous_ast = {"<class>": "root", "elements": [{"<class>": "bool", "name": {"<class>": "scope_name", "ids": ["bool"]}}, {"<class>": "void", "name": {"<class>": "scope_name", "ids": ["void"]}}, {"<class>": "file-name", "name": "foo.dzn"}, {"<class>": "interface", "name": {"<class>": "scope_name", "ids": ["ihello"]}, "types": {"<class>": "types", "elements": []}, "events": {"<class>": "events", "elements": [{"<class>": "event", "name": "hello", "signature": {"<class>": "signature", "type_name": {"<class>": "scope_name", "ids": ["void"]}, "formals": {"<class>": "formals", "elements": []}}, "direction": "in"}, {"<class>": "event", "name": "world", "signature": {"<class>": "signature", "type_name": {"<class>": "scope_name", "ids": ["void"]}, "formals": {"<class>": "formals", "elements": []}}, "direction": "out"}]}}, {"<class>": "interface", "name": {"<class>": "scope_name", "ids": ["itimer"]}, "types": {"<class>": "types", "elements": []}, "events": {"<class>": "events", "elements": [{"<class>": "event", "name": "hello", "signature": {"<class>": "signature", "type_name": {"<class>": "scope_name", "ids": ["void"]}, "formals": {"<class>": "formals", "elements": []}}, "direction": "in"}, {"<class>": "event", "name": "world", "signature": {"<class>": "signature", "type_name": {"<class>": "scope_name", "ids": ["void"]}, "formals": {"<class>": "formals", "elements": []}}, "direction": "out"}]}}, {"<class>": "component", "name": {"<class>": "scope_name", "ids": ["hello"]}, "ports": {"<class>": "ports", "elements": [{"<class>": "port", "name": "hh", "type_name": {"<class>": "scope_name", "ids": ["ihello"]}, "direction": "provides", "formals": {"<class>": "formals", "elements": []}}, {"<class>": "port", "name": "tt", "type_name": {"<class>": "scope_name", "ids": ["itimer"]}, "direction": "requires", "formals": {"<class>": "formals", "elements": []}}]}}, {"<class>": "system", "name": {"<class>": "scope_name", "ids": ["hello_system"]}, "ports": {"<class>": "ports", "elements": [{"<class>": "port", "name": "h", "type_name": {"<class>": "scope_name", "ids": ["ihello"]}, "direction": "provides", "formals": {"<class>": "formals", "elements": []}}]}, "instances": {"<class>": "instances", "elements": [{"<class>": "instance", "name": "c", "type_name": {"<class>": "scope_name", "ids": ["hello"]}}, {"<class>": "instance", "name": "t", "type_name": {"<class>": "scope_name", "ids": ["timer"]}}]}, "bindings": {"<class>": "bindings", "elements": [{"<class>": "binding", "left": {"<class>": "end-point", "port_name": "h"}, "right": {"<class>": "end-point", "instance_name": "c", "port_name": "hh"}}, {"<class>": "binding", "left": {"<class>": "end-point", "instance_name": "c", "port_name": "tt"}, "right": {"<class>": "end-point", "instance_name": "t", "port_name": "t"}}]}}], "working-directory": "/home/janneke/src/verum/dzn/wip"};

//let AST = hello_ast;
//let AST = system_hello_ast;
//let AST = erroneous_ast;
let AST = Camera_ast;
